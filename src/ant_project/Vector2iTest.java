package ant_project;

import ant_project.Vector2i;
import junit.framework.TestCase;

public class Vector2iTest extends TestCase {


    public void testConstructor() throws Exception {
        {
            Vector2i testVector = new Vector2i();
            assertEquals(testVector.x, 0);
            assertEquals(testVector.y, 0);
        }
        {
            Vector2i testVector = new Vector2i(1, 1);
            assertEquals(testVector.x, 1);
            assertEquals(testVector.y, 1);
        }
        {
            Vector2i testVector = new Vector2i(100000, -1000000);
            assertEquals(testVector.x, 100000);
            assertEquals(testVector.y, -1000000);
        }
    }


    public void testSet() throws Exception {
        {
            Vector2i testVector = new Vector2i();
            testVector.set(1, 1);
            assertEquals(testVector.x, 1);
            assertEquals(testVector.y, 1);
        }
        {
            Vector2i testVector = new Vector2i();
            testVector.set(100000, -1000000);
            assertEquals(testVector.x, 100000);
            assertEquals(testVector.y, -1000000);
        }

    }
}