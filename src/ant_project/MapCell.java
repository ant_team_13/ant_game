package ant_project;

import java.security.InvalidParameterException;

/**
 * Created by Daniel Saska on 22/02/2015.
 */
public class MapCell {

    private Vector2i pos;
    private int food;
    private Ant ant;
    private Terrain terrain;
    private boolean[] blackMarker = new boolean[6];
    private boolean[] redMarker = new boolean[6];

    /**
     * Initialises the map cell
     * @param token Valid token signifying the nature of the map cell
     * @param pos Position of the map cell on map
     */
    public MapCell(String token, Vector2i pos) {
        this.pos = new Vector2i(pos.x, pos.y);
        boolean isInt = true;
        try {
            Integer.parseInt(token);
        } catch(NumberFormatException e) {
            isInt = false;
        }

        if (token.equals(".")) {
            terrain = Terrain.CLEAR;
        } else if (token.equals("#")) {
            terrain = Terrain.ROCKY;
        } else if (token.equals("+")) {
            terrain = Terrain.RED_ANTHILL;
        } else if (token.equals("-")) {
            terrain = Terrain.BLACK_ANTHILL;
        } else if (isInt && Integer.parseInt(token) > 0) {
            terrain = Terrain.CLEAR;
            food = Integer.parseInt(token);
        } else {
            throw new IllegalArgumentException("\"" + token + "\" is not valid token for constructing Map Cell");
        }
    }

    /**
     * Returns the amount of food on the cell.
     * @return Amount of food on the cell
     */
    public int getFoodAmount() {
        return (int) food;
    }

    /**
     * Method signifying whether the terrain is rocky or not.
     * @return True if terrain is rocky, false otherwise
     */
    public boolean isRocky() {
        return terrain == Terrain.ROCKY;
    }

    /**
     * Method signifying whether the terrain contains anthill or not.
     * @return True if terrain contains anthill, false otherwise
     */
    public boolean containsAnt() {
        return ant != null;
    }

    /**
     * Method signifying whether the terrain contains ant of specified color or not
     * @param color Color of the ant
     * @return True if cell contains ant of specified color, false otherwise
     */
    public boolean containsAnt(Ant.TeamColor color) { return ant != null && color == ant.getColor(); }

    /**
     * Returns the ant on the map cell
     * @return Ant positioned on this map cell
     */
    public Ant getAnt() {
        return ant;
    }

    /**
     * Assigns ant to this map cell.
     * @param ant Ant to be positioned on the map cell
     */
    public void setAnt(Ant ant) {
        this.ant = ant;
    }

    /**
     * Clears the ant from the map cell (if any).
     */
    public void clearAnt() {
        this.ant = null;
    }

    /**
     * Enumeration describing different types of terrain the map cell can contain
     */
    enum Terrain {
        CLEAR, ROCKY, RED_ANTHILL, BLACK_ANTHILL;

        /**
         * Returns the string token representation of the terrain type
         * @return String token representation
         */
        @Override
        public String toString() {

            String str = "";

            if(name().equals("CLEAR")) {
                return ".";
            } else if (name().equals("ROCKY")) {
                str = "#";
            } else if (name().equals("RED_ANTHILL")) {
                str = "+";
            } else if (name().equals("BLACK_ANTHILL")) {
                str = "-";
            }

            return str;
        }
    }

    /**
     * Returns the string token representation of the map cell
     * @return String token representation
     */
    @Override
    public String toString() {

        if (food == 0) {
            return terrain.toString();
        } else if (food < 10) {
            return String.valueOf((int) food);
        } else {
            throw new InvalidParameterException("Map contains value not supported by file format on cell ["
                    + Integer.toString(pos.x) + ", " + Integer.toString(pos.y) + "] where food amount is "
                    + Integer.toString(food));
        }

    }

    /**
     * Signifies whether or not the map cell contains anthill of specified color
     * @param color Color of the anthill
     * @return True if the map cell is anthill of given color, false otherwise
     */
    public boolean isAnthill(Ant.TeamColor color) {
        if (color == Ant.TeamColor.RED) {
            return (terrain == Terrain.RED_ANTHILL);
        } else {
            return (terrain == Terrain.BLACK_ANTHILL);
        }
    }

    /**
     * Signifies whether or not the map cell is tagged with marker of specified color and ID
     * @param color Color of the marker
     * @param markerId ID of the marker
     * @return True if the map cell contains specified marker, false otherwise
     */
    public boolean checkMarker(Ant.TeamColor color, int markerId) {
        if (color == Ant.TeamColor.BLACK) {
            if(markerId >= blackMarker.length) { return false; }
            return blackMarker[markerId];
        }
        else if (color == Ant.TeamColor.RED) {
            if(markerId >= redMarker.length) { return false; }
            return redMarker[markerId];
        }
        return false;
    }

    /**
     * Signifies whether or not the map cell is tagged with marker of specified color
     * @param color Color of the marker
     * @return True if the map cell contains specified marker, false otherwise
     */
    public boolean checkMarker(Ant.TeamColor color) {
        if (color == Ant.TeamColor.BLACK) {
            for (boolean m : blackMarker) { if (m) { return true; } }
        }
        else if (color == Ant.TeamColor.RED) {
            for (boolean m : redMarker) { if (m) { return true; } }
        }
        return false;
    }

    /**
     * Sets the food amount to that provided in parameter
     * @param foodAmount Amount of Food
     */
    public void setFoodAmount(int foodAmount) {
        this.food = (char) foodAmount;
    }

    /**
     * Tags the map cell with marker of specified color and ID
     * @param color Color of the marker
     * @param markerId ID of the marker
     */
    public void setMarker(Ant.TeamColor color, int markerId) {
        if (color == Ant.TeamColor.BLACK) {
            if(markerId >= blackMarker.length || markerId < 0) {
                throw new InvalidParameterException("Unexpected error occurred: Marker ID cannot be "
                        + Integer.toString(markerId));
            }
            blackMarker[markerId] = true;
        }
        else if (color == Ant.TeamColor.RED) {
            if(markerId >= redMarker.length || markerId < 0) {
                throw new InvalidParameterException("Unexpected error occurred: Marker ID cannot be "
                        + Integer.toString(markerId));
            }
            redMarker[markerId] = true;
        }
    }

    /**
     * Clears the marker of specified color and ID
     * @param color Color of the marker
     * @param markerId ID of the marker
     */
    public void clearMarker(Ant.TeamColor color, int markerId) {
        if (color == Ant.TeamColor.BLACK) {
            if(markerId >= blackMarker.length) {
                throw new InvalidParameterException("Unexpected error occurred: Marker ID cannot be "
                        + Integer.toString(markerId));
            }
            blackMarker[markerId] = false;
        }
        else if (color == Ant.TeamColor.RED) {
            if(markerId >= redMarker.length) {
                throw new InvalidParameterException("Unexpected error occurred: Marker ID cannot be "
                        + Integer.toString(markerId));
            }
            redMarker[markerId] = false;
        }
    }

    /**
     * Returns position of the map cell
     * @return Position of the map cell
     */
    public Vector2i getPosition() {
        return pos;
    }

    /**
     * Signifies whether or not the terrain is clear
     * @return True if clear, false otherwise
     */
    public boolean isClear() {
        return terrain == Terrain.CLEAR;
    }
}
