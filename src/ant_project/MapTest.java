package ant_project;

import ant_project.*;
import ant_project.fsa.Automaton;
import junit.framework.TestCase;

/**
 * Created by Luke on 30/03/2015.
 */
public class MapTest extends TestCase {

    private String input = "10\n" +
            "10\n" +
            "# # # # # # # # # #\n" +
            " # 9 9 . . . . 3 3 #\n" +
            "# 9 # . - - - - - #\n" +
            " # . # - - - - - - #\n" +
            "# . . 5 - - - - - #\n" +
            " # + + + + + 5 . . #\n" +
            "# + + + + + + # . #\n" +
            " # + + + + + . # 9 #\n" +
            "# 3 3 . . . . 9 9 #\n" +
            " # # # # # # # # # #";
    private String input2 = "10\n" +
            "9\n" +
            "# # # # # # # # # #\n" +
            " # 9 9 . . . . 3 3 #\n" +
            "# 9 # . - - - - - #\n" +
            " # . # - - - - - - #\n" +
            "# . . 5 - - - - - #\n" +
            " # + + + + + 5 . . #\n" +
            "# + + + + + + # . #\n" +
            " # 3 3 . . . . 9 9 #\n" +
            "# # # # # # # # # #";

    public void testToString() throws Exception {
        Map testMap = new Map(input);
        String testOutput = testMap.toString();
        assertEquals(input, testOutput);
    }

    public void testGetAdjacentAnts() throws Exception {
        Map testMap = new Map(input);
        AntManager am = new AntManager(new Automaton("Flip 1 0 0"), new Automaton("Flip 1 0 0"), testMap);
        assertEquals(testMap.getAdjacentAnts(new Vector2i(4,4), Ant.TeamColor.RED),2);
        assertEquals(testMap.getAdjacentAnts(new Vector2i(4,4), Ant.TeamColor.BLACK),3);
        assertEquals(testMap.getAdjacentAnts(new Vector2i(4,3),Ant.TeamColor.RED),0);
        assertEquals(testMap.getAdjacentAnts(new Vector2i(4,3), Ant.TeamColor.BLACK),6);
        assertEquals(testMap.getAdjacentAnts(new Vector2i(4,5),Ant.TeamColor.RED),4);
        assertEquals(testMap.getAdjacentAnts(new Vector2i(4,5), Ant.TeamColor.BLACK),2);
        assertEquals(testMap.getAdjacentAnts(new Vector2i(3,1),Ant.TeamColor.RED),0);
        assertEquals(testMap.getAdjacentAnts(new Vector2i(3,1), Ant.TeamColor.BLACK),1);
    }

    public void testGetMapCellInDir() throws Exception {
        Map testMap = new Map(input);
        Vector2i pos = new Vector2i(4,4);
        Ant testAnt = new Ant(Ant.TeamColor.RED);
        testAnt.setDirection(Ant.Direction.fromInt(5));
        Ant.Direction dir = testAnt.getDirection();

        assertEquals(testMap.getMapCellInDir(pos,dir),testMap.getCell(4,3));
        assertNotSame(testMap.getMapCellInDir(pos,dir),testMap.getCell(4,4));

    }

    public void testGetCell() throws Exception {
        Map testMap = new Map(input);
        assertEquals(testMap.getCell(4,4),testMap.getMap()[4][4]);
        assertNotSame(testMap.getCell(4,4),testMap.getMap()[4][3]);
        assertEquals(testMap.getCell(3,3),testMap.getMap()[3][3]);
        assertNotSame(testMap.getCell(3,3),testMap.getMap()[4][4]);
        assertEquals(testMap.getCell(4,3),testMap.getMap()[4][3]);
        assertNotSame(testMap.getCell(4,3),testMap.getMap()[4][4]);

    }

    public void testGetCellBL() throws Exception {
        Map testMap = new Map(input);
        MapCell temp = new MapCell("+",new Vector2i(4,5));
        assertEquals(testMap.getCellBL(4,4).getPosition().y, 5);
        assertEquals(testMap.getCellBL(4,5).getPosition().y, 4);
        assertEquals(testMap.getCellBL(4,6).getPosition().y, 3);
        assertEquals(testMap.getCellBL(4,7).getPosition().y, 2);
    }

    public void testIsEvenBL() throws Exception {
        {
            Map testMap = new Map(input);
            assertTrue(testMap.isEvenBL(10));
            assertFalse(testMap.isEvenBL(9));
            assertTrue(testMap.isEvenBL(0));
        }
        {
            Map testMap = new Map(input2);
            assertTrue(testMap.isEvenBL(9));
            assertFalse(testMap.isEvenBL(8));
            assertFalse(testMap.isEvenBL(0));
        }
    }

}