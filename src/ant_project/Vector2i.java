/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project;

/**
 * Provides basic functionality of 2D vector for storing position
 */
public class Vector2i {
    public int x;
    public int y;

    /**
     * Constructs vector from values in individual dimensions
     * @param x X value of the vector
     * @param y Y value of the vector
     */
    Vector2i(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Zero-initializes the vector
     */
    Vector2i() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * Sets the value of the vector
     * @param x X value of the vector
     * @param y Y value of the vector
     */
    void set(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
