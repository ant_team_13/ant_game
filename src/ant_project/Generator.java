package ant_project;

import java.util.Random;

/**
 * Class used for generating random World Map Terrain
 */
public class Generator {
    private static final int NUM_RETRIES = 100;

    /**
     * Generates the map terrain from specified parameters
     * @param rand Predefined random generator
     * @param sideLength Side length of each anthill
     * @param numFoodBlobs Number of food blobs on the map.
     * @param minBlobSize Minimum length of blob side
     * @param maxBlobSize Maximum length of blob side
     * @param minFoodPerCell Minimum food amount in single cell of a food blob
     * @param maxFoodPerCell Maximum food amount in single cell of a food blob
     * @param numRocks Number of rocks on the map.
     * @param width Width of the map.
     * @param height Height of the map.
     * @return Map constructed from the parameters provided.
     */
    public static Map generate(Random rand,
                          int sideLength,
                          int numFoodBlobs,
                          int minBlobSize, int maxBlobSize,
                          int minFoodPerCell, int maxFoodPerCell,
                          int numRocks,
                          int width, int height ) {
        Map map = new Map(width, height);
        boolean full[] = new boolean[width * height];

        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                if (x == 0 || y == 0 || x == width - 1 || y == height - 1) {
                    full[y * width + x] = true;
                    map.getTerrain()[x][y] = new MapCell("#", new Vector2i(x,y));
                } else if (x == 1 || y == 1 || x == width - 2 || y == height - 2) {
                    full[y * width + x] = true;
                } else {
                    full[y * width + x] = false;
                }
            }
        }
        drawAnthill(rand, sideLength, map, full, Ant.TeamColor.BLACK);
        drawAnthill(rand, sideLength, map, full, Ant.TeamColor.RED);

        for (int blob = 0; blob < numFoodBlobs; ++blob) {
            int type = Math.abs(rand.nextInt()) % 3;
            int size = Math.abs(rand.nextInt()) % (maxBlobSize - minBlobSize + 1) + minBlobSize;
            if  (type == 0) {
                if(!drawFoodBlobA(rand, map, full, size, minFoodPerCell, maxFoodPerCell)){
                    if(!drawFoodBlobB(rand, map, full, size, minFoodPerCell, maxFoodPerCell)) {
                        if(!drawFoodBlobC(rand, map, full, size, minFoodPerCell, maxFoodPerCell))
                        { throw new IllegalArgumentException("Not enough space found to draw all food blobs with this seed."); }
                    }
                }
            } else if (type == 1) {
                if(!drawFoodBlobB(rand, map, full, size, minFoodPerCell, maxFoodPerCell)){
                    if(!drawFoodBlobC(rand, map, full, size, minFoodPerCell, maxFoodPerCell)) {
                        if(!drawFoodBlobA(rand, map, full, size, minFoodPerCell, maxFoodPerCell))
                        { throw new IllegalArgumentException("Not enough space found to draw all food blobs with this seed."); }
                    }
                }
            } else if (type == 2) {
                if(!drawFoodBlobC(rand, map, full, size, minFoodPerCell, maxFoodPerCell)){
                    if(!drawFoodBlobA(rand, map, full, size, minFoodPerCell, maxFoodPerCell)) {
                        if(!drawFoodBlobB(rand, map, full, size, minFoodPerCell, maxFoodPerCell))
                        { throw new IllegalArgumentException("Not enough space found to draw all food blobs with this seed."); }
                    }
                }
            }
        }

        for (int rock = 0; rock < numRocks; ++rock) {
            drawRock(rand, map, full);
        }

        for (int y = 0; y < map.getHeight(); ++y) {
            for (int x = 0; x < map.getWidth(); ++x) {
                if (map.getTerrain()[x][y] == null) {
                    map.getTerrain()[x][y] = new MapCell(".", new Vector2i(x,y));
                }
            }
        }
        return map;
    }

    /**
     * Draws Anthill of specified color
     * @param rand Predefined random generator
     * @param sideLength Side length of each anthill
     * @param map Map to draw the anthill on
     * @param full Array of filled cells
     * @param color Color of the anthill
     */
    private static void drawAnthill(Random rand, int sideLength, Map map, boolean full[], Ant.TeamColor color) {
        String token = "+";
        if (color == Ant.TeamColor.BLACK)
        { token = "-"; }

        boolean success = false;
        for (int i = 0; i < NUM_RETRIES; ++i) {
            int coordY = Math.abs(rand.nextInt()) % map.getHeight();
            int coordX = Math.abs(rand.nextInt()) % map.getWidth();

            //Verify that cells are free

            {
                success = true;
                //Check if we are within bounds of the map
                if (coordY - sideLength + 1 < 0
                        || coordY + sideLength - 1 >= map.getHeight()
                        || coordX + (sideLength - 1) * 2 >= map.getWidth()) {
                    success = false; //Not enough space
                }

                for (int x = 0; x <= (sideLength - 1) * 2 + 1; ++x) {
                    if (full[coordY * map.getWidth() + x + coordX]) {
                        success = false;
                        break;
                    } //Not enough space
                }
                int xOffset = 0;
                for (int y = 1; y <= sideLength - 1 && success; ++y) {
                    if ((coordY + y) % 2 == 0) {
                        ++xOffset;
                    }
                    for (int x = 0; x < (sideLength - 1) * 2 - y + 1; ++x) {
                        if (full[(coordY + y) * map.getWidth() + x + coordX + xOffset]) {
                            success = false;
                            break;
                        } //Not enough space
                        if (full[(coordY - y) * map.getWidth() + x + coordX + xOffset]) {
                            success = false;
                            break;
                        } //Not enough space
                    }
                }
            }

            //Draw in anthill
            if (success)
            {
                for (int x = -1; x <= (sideLength - 1) * 2 + 1; ++x) {
                    full[coordY * map.getWidth() + x + coordX] = true;
                    if (x >= 0 && x < (sideLength - 1) * 2 + 1) {
                        map.getTerrain()[x + coordX][coordY]
                                = new MapCell(token, new Vector2i(x + coordX, coordY));
                    }
                }
                int xOffset = 0;
                for (int y = 1; y <= sideLength; ++y) {
                    if ((coordY + y) % 2 == 0) {
                        ++xOffset;
                    }

                    for (int x = -1; x <= (sideLength - 1) * 2 - y + 1; ++x) {
                        full[(coordY + y) * map.getWidth() + x + coordX + xOffset] = true;
                        full[(coordY - y) * map.getWidth() + x + coordX + xOffset] = true;
                        if (x >= 0 && x < (sideLength - 1) * 2 - y + 1&& y < sideLength) {
                            map.getTerrain()[x + coordX + xOffset][coordY + y]
                                    = new MapCell(token, new Vector2i(x + coordX + xOffset, coordY + y));
                            map.getTerrain()[x + coordX + xOffset][coordY - y]
                                    = new MapCell(token, new Vector2i(x + coordX + xOffset, coordY - y));
                        }
                    }
                }
                break;
            }
        }
        if (!success) {
            throw new IllegalArgumentException("Not enough space found to draw all anthills with this seed.");
        }
    }

    /**
     * Draws food blob of Type A (No slant)
     * @param rand Predefined random generator
     * @param map Map to draw the anthill on
     * @param full Array of filled cells
     * @param blobSize Size of the blob to draw
     * @param minFoodPerCell Minimum food amount in single cell of a food blob
     * @param maxFoodPerCell Maximum food amount in single cell of a food blob
     * @return returns true if the food blob have been drawn successfully, false otherwise
     */
    private static boolean drawFoodBlobA(Random rand, Map map, boolean full[], int blobSize, int minFoodPerCell, int maxFoodPerCell) {
        boolean success = false;
        for (int i = 0; i < NUM_RETRIES; ++i) {
            int coordY = Math.abs(rand.nextInt()) % map.getHeight();
            int coordX = Math.abs(rand.nextInt()) % map.getWidth();

            //Test
            {
                success = true;
                if (coordX + blobSize > map.getWidth() || coordY + blobSize > map.getHeight()) {
                    success = false; //Not enough space
                }
                for (int y = 0; y < blobSize && success; ++y) {
                    for (int x = 0; x < blobSize; ++x) {
                        if(full[(coordY + y) * map.getWidth() + x + coordX]) {
                            success = false;
                            break;
                        } //Not enough space
                    }
                }
            }

            if (success)
            {
                for (int y = 0; y < blobSize; ++y) {
                    for (int x = 0; x < blobSize; ++x) {
                        full[(coordY + y) * map.getWidth() + x + coordX] = true;
                        String token = Integer.toString(Math.abs((rand.nextInt()) % (maxFoodPerCell - minFoodPerCell + 1)) + minFoodPerCell);
                        map.getTerrain()[x + coordX][coordY + y]
                                = new MapCell(token, new Vector2i(x + coordX, coordY + y));
                    }
                }
                break;
            }
        }
        return success;
    }

    /**
     * Draws food blob of Type B (Slant 1)
     * @param rand Predefined random generator
     * @param map Map to draw the anthill on
     * @param full Array of filled cells
     * @param blobSize Size of the blob to draw
     * @param minFoodPerCell Minimum food amount in single cell of a food blob
     * @param maxFoodPerCell Maximum food amount in single cell of a food blob
     * @return returns true if the food blob have been drawn successfully, false otherwise
     */
    private static boolean drawFoodBlobB(Random rand, Map map, boolean full[], int blobSize, int minFoodPerCell, int maxFoodPerCell) {
        boolean success = false;
        for (int i = 0; i < NUM_RETRIES; ++i) {
            int coordY = Math.abs(rand.nextInt()) % map.getHeight();
            int coordX = Math.abs(rand.nextInt()) % map.getWidth();

            //Test
            {
                success = true;
                int xOffset = 0;
                for (int y = 1; y <= blobSize - 1; ++y) {
                    if ((coordY + y) % 2 == 0) {  ++xOffset; }
                }

                if (coordX + blobSize + xOffset > map.getWidth() || coordY + blobSize > map.getHeight()) {
                    success = false; //Not enough space
                }

                xOffset = 0;
                for (int y = 0; y < blobSize && success; ++y) {
                    if ((coordY + y) % 2 == 0 && y != 0) {  ++xOffset; }
                    for (int x = 0; x < blobSize; ++x) {
                        if(full[(coordY + y) * map.getWidth() + x + coordX + xOffset]) {
                            success = false;
                            break;
                        } //Not enough space
                    }
                }
            }

            if (success)
            {
                int xOffset = 0;
                for (int y = 0; y < blobSize; ++y) {
                    if ((coordY + y) % 2 == 0 && y != 0) {  ++xOffset; }
                    for (int x = 0; x < blobSize; ++x) {
                        full[(coordY + y) * map.getWidth() + x + coordX + xOffset] = true;
                        String token = Integer.toString((Math.abs(rand.nextInt()) % (maxFoodPerCell - minFoodPerCell + 1)) + minFoodPerCell);
                        map.getTerrain()[x + coordX + xOffset][coordY + y]
                                = new MapCell(token, new Vector2i(x + coordX, coordY + y));
                    }
                }
                break;
            }
        }
        return success;
    }


    /**
     * Draws food blob of Type C (Slant 2)
     * @param rand Predefined random generator
     * @param map Map to draw the anthill on
     * @param full Array of filled cells
     * @param blobSize Size of the blob to draw
     * @param minFoodPerCell Minimum food amount in single cell of a food blob
     * @param maxFoodPerCell Maximum food amount in single cell of a food blob
     * @return returns true if the food blob have been drawn successfully, false otherwise
     */
    private static boolean drawFoodBlobC(Random rand, Map map, boolean full[], int blobSize, int minFoodPerCell, int maxFoodPerCell) {
        boolean success = false;
        for (int i = 0; i < NUM_RETRIES; ++i) {
            int coordY = Math.abs(rand.nextInt()) % map.getHeight();
            int coordX = Math.abs(rand.nextInt()) % map.getWidth();

            //Test
            {
                success = true;
                int xOffset = 0;
                for (int y = 1; y <= blobSize - 1; ++y) {
                    if ((coordY + y) % 2 == 0) {  ++xOffset; }
                }

                if (coordX + blobSize + xOffset > map.getWidth() || coordY - blobSize < -1) {
                    success = false; //Not enough space
                }

                xOffset = 0;
                for (int y = 0; y < blobSize && success; ++y) {
                    if ((coordY + y) % 2 == 0 && y != 0) {  ++xOffset; }
                    for (int x = 0; x < blobSize; ++x) {
                        if(full[(coordY - y) * map.getWidth() + x + coordX + xOffset]) {
                            success = false;
                            break;
                        } //Not enough space
                    }
                }
            }

            if (success)
            {
                int xOffset = 0;
                for (int y = 0; y <= blobSize - 1; ++y) {
                    if ((coordY + y) % 2 == 0 && y != 0) {  ++xOffset; }
                    for (int x = 0; x < blobSize; ++x) {
                        full[(coordY - y) * map.getWidth() + x + coordX + xOffset] = true;
                        String token = Integer.toString((Math.abs(rand.nextInt()) % (maxFoodPerCell - minFoodPerCell + 1)) + minFoodPerCell);
                        map.getTerrain()[x + coordX + xOffset][coordY - y]
                                = new MapCell(token, new Vector2i(x + coordX, coordY + y));
                    }
                }
                break;
            }
        }
        return success;
    }

    //Draws 1x1 rock

    /**
     * Draws 1x1 rock
     * @param rand Predefined random generator
     * @param map Map to draw the anthill on
     * @param full Array of filled cells
     */
    private static void drawRock(Random rand, Map map, boolean full[] ) {
        int emptyCount = 0;
        for (int i = 0; i < full.length; ++i) {
            if (!full[i]) { ++emptyCount; }
        }
        if (emptyCount == 0) {
            throw new IllegalArgumentException("Not enough space found to draw all rocks with this seed.");
        }

        int rockIndex = Math.abs(rand.nextInt()) % emptyCount;
        int x = 0;
        int y = 0;
        boolean done = false;
        for (; y < map.getHeight(); ++y) {
            for (; x < map.getWidth(); ++x) {
                if (full[y * map.getWidth() + x] == false) {
                    if(rockIndex-- == 0) {
                        done = true;
                        break;
                    }
                }
            }
            if (done) { break; }
            x = 0;
        }

        map.getTerrain()[x][y] = new MapCell("#", new Vector2i(x,y));

        for (int xn = x - 1; xn <= x+1; ++xn)
        { full[y * map.getWidth() + xn] = true; }


        int xOffset = 0;
        if (y % 2 == 1) {
            ++xOffset;
        }
        for (int xn = -1; xn < 1; ++xn) {
            full[(y + 1) * map.getWidth() + x + xn + xOffset] = true;
            full[(y - 1) * map.getWidth() + x + xn + xOffset] = true;
        }
    }
}
