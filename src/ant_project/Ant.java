/**
 * Created by Daniel Saska on 22/02/2015
 */
package ant_project;

public class Ant {

    private TeamColor color;
    private boolean hasFood;
    private MapCell position;
    private short state;
    private Direction direction;
    private int resting;

    /**
     * Method indicating whether ant has any food or not.
     * @return Returns true if ant has food, false otherwise.
     */
    public boolean hasFood() {
        return hasFood;
    }

    /**
     * Returns ant's position in form a Map Cell
     * @return Ant's Position
     */
    public MapCell getPosition() {
        return position;
    }

    /**
     * Sets the next state ID of execution to be that specified in first parameter
     * @param state Next State ID
     */
    public void setState(short state) {
        this.state = state;
    }

    /**
     * Returns next State ID of execution
     * @return Next State ID
     */
    public short getState() {
        return state;
    }

    /**
     * Sets ant to have food (or not)
     * @param hasFood Indicates whether ant carries piece of food (true) or not (false).
     */
    public void setHasFood(boolean hasFood) {
        this.hasFood = hasFood;
    }

    /**
     * Returns direction in which ant is currently facing
     * @return Direction of ant
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     *  Sets facing direction of the ant to be that specified in first parameter
     * @param direction Direction of the ant
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * Sets the position of ant to be that specified in first parameter.
     * @param position Position of the ant
     */
    public void setPosition(MapCell position) {
        this.position = position;
    }

    /**
     * Sets the time for which the ant has to be resting before executing next automaton state.
     * @param resting Resting time
     */
    public void setResting(int resting) {
        this.resting = resting;
    }

    /**
     * Returns the time for which the ant has to be resting before executing next automaton state.
     * @return Resting time
     */
    public int getResting() {
        return resting;
    }

    /**
     * Returns team color of the ant
     * @return Team Color of the ant
     */
    public TeamColor getColor() {
        return color;
    }

    /**
     * Enumeration defining possible ant colors the ant can be associated with
     */
    public enum TeamColor {
        RED, BLACK;

        /**
         * Returns opposite of the the given color
         * @param color The given color
         * @return The opposite color
         */
        public static TeamColor opposite(TeamColor color) {
            if(color == RED) {
                return BLACK;
            } else {
                return RED;
            }
        }

        /**
         * Returns the opposite of the color of the Ant
         * @return The opposite color
         */
        public TeamColor opposite() {
            if(this == RED) {
                return BLACK;
            } else {
                return RED;
            }
        }
    }

    /**
     * Class specifying the direction in which ant is facing.
     */
    public static class Direction {

        /**
         * Char is used to save space, as we only have 6 possible directions we only need 1 byte to store any one of them,
         * not the 4 bytes an int would take up
         */
        private char dir;

        /**
         * Constructor that assigns a value to dir, from the given Integer value
         * @param dir Given Integer value
         */
        public Direction(int dir) {
            this.dir = (char) dir;
        }

        /**
         * Returns the current direction
         * @return The current direction
         */
        public int getDir() {
            return (int) this.dir;
        }

        /**
         * Rotates the current position left
         */
        public void rotateLeft() {
            this.dir = (char) getLeft().getDir();
        }

        /**
         * Rotates the current position right
         */
        public void rotateRight() {
            this.dir = (char) getRight().getDir();
        }

        /**
         * Returns Direction that corresponds the the given Integer value
         * @param x Given Integer
         * @return Direction that corresponds to given Integer value
         */
        public static Direction fromInt(int x) {
            return new Direction(x);
        }

        /**
         * Returns the Direction directly Left of the current Direction
         * @return Direction directly Left of current Direction
         */
        public Direction getLeft() {
            // (0 + 5) % 6 = 5 % 6 = 6  ;   (1 + 5) % 6 = 6 % 6 = 0 ;   (2 + 5) % 6 = 7 % 6 = 1
            return new Direction((getDir()+5) % 6);
        }

        /**
         * Returns the Direction directly Right of the current Direction
         * @return Direction directly Right of current Direction
         */
        public Direction getRight() {
            // (0 + 1) % 6 = 1 % 6 = 1  ;   (1 + 1) % 6 = 2 % 6 = 2 ;   (5 + 1) % 6 = 6 % 6 = 0
            return new Direction((getDir()+1) % 6);
        }

    }

    /**
     * Constructor that creates an Ant and assigns it a specific color
     * @param color Color of the Ant that has been created
     */
    public Ant(TeamColor color) {
        this.color = color;
        hasFood = false;
        direction = Direction.fromInt(0);
        state = 0;
        resting = 0;
    }

    /**
     * Method to be called when he is surrounded by enemies and dies.
     */
    public void kill() {
        position.clearAnt();
        position.setFoodAmount(position.getFoodAmount() + 3 + (hasFood ? 1 : 0));
        position = null;
    }
}