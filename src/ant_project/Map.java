/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project;

import java.util.Arrays;

/**
 * The Map class stores an array of MapCells as well as map dimensions. It provides functions
 * to load and save map files and to ease the movement of the ants in the world.
 *
 * @author calum & luke
 */
public class Map {

    //map call grid - the world
    private MapCell[][] mapArray;

    /**
     * Pre-initializes map with no parameters
     */
    public Map() {
    }

    /**
     * Constructs empty map of specified dimensions
     * @param width Width of the map
     * @param height Height of the map
     */
    public Map(int width, int height) {
        mapArray = new MapCell[width][height];
    }
    
    /**
     * Creates map from content of map file
     * @param fileContent Content of the file
     */
    public Map(String fileContent) {
        buildMap(fileContent);
    }

    /**
     * Converts the current state of the map into a well-formed equivalent string
     * in the form of an Ant World Map file. Layout defined in customer spec.
     *
     * @return String representation of Ant World
     */
    @Override
    public String toString() {
        String ret = "";
        ret += Integer.toString(mapArray.length ) + "\n";
        ret += Integer.toString(mapArray[0].length) + "\n";
        for(int i = 0; i < mapArray[0].length; ++i) {
            if(i % 2 == 1) { ret += " "; } //Even cell has leading space
            for (int j = 0; j < mapArray.length; ++j) {
                ret += mapArray[j][i].toString() + " ";
            }
            if (mapArray.length > 0) { ret = ret.substring(0, ret.length() - 1); } //Strip last space
            ret += "\n";
        }
        if (mapArray.length > 0) { ret = ret.substring(0, ret.length() - 1); } //Strip last break
        return ret;
    }

    /**
     * Explores adjacent cells to the one provided via its Vector2i location and
     * counts the number of ants of the given color on those cells.
     *
     * @param position target cell in form of Vector2i location
     * @param color    target ant color e.g. RED or BLACK
     * @return count of adjacent ants in character form
     */
    public char getAdjacentAnts(Vector2i position, Ant.TeamColor color) {
        char count = 0;
        for (int i = 0; i < 6; i++) {
            Ant.Direction d = Ant.Direction.fromInt(i);
            MapCell cell = getMapCellInDir(position, d);
            if (cell.containsAnt() && cell.getAnt().getColor() == color) {
                count++;
            }
        }
        return count;
    }

    /**
     * Explores adjacent cells to the one provided via its MapCell instance and
     * counts the number of ants of the given color on those cells.
     *
     * @param position target cell in form of MapCell instance
     * @param color    target ant color e.g. RED or BLACK
     * @return count of adjacent ants in character form
     */
    public char getAdjacentAnts(MapCell position, Ant.TeamColor color) {
        return getAdjacentAnts(position.getPosition(), color);
    }

    /**
     * Attempts to find the MapCell in the given direction an ant is facing from the cell provided
     * via its Vector2i location. If the cell would be outside the map boundary, null is returned.
     *
     * @param position target cell in form of Vector2i location
     * @param dir      Ant instance current direction
     * @return the MapCell an ant is looking at. Null if looking out-of-bounds
     */
    public MapCell getMapCellInDir(Vector2i position, Ant.Direction dir) {

        Vector2i dirPos = new Vector2i();
        dirPos.x = position.x;
        dirPos.y = position.y;
        switch (dir.getDir()) {
            case 0:
                dirPos.x += 1;
                break;
            case 1:
                dirPos.y += 1;
                dirPos.x += ((position.y % 2 == 0) ? 0 : 1);
                break;
            case 2:
                dirPos.y += 1;
                dirPos.x -= ((position.y % 2 == 0) ? 1 : 0);
                break;
            case 3:
                dirPos.x -= 1;
                break;
            case 4:
                dirPos.y -= 1;
                dirPos.x -= ((position.y % 2 == 0) ? 1 : 0);
                break;
            case 5:
                dirPos.y -= 1;
                dirPos.x += ((position.y % 2 == 0) ? 0 : 1);
                break;
            default:
        }
        if (dirPos.x < 0 || dirPos.y < 0 || dirPos.x >= mapArray.length || dirPos.y >= mapArray[0].length) { return null; }
        return mapArray[dirPos.x][dirPos.y];
    }

    /**
     * Attempts to find the MapCell in the given direction an ant is facing from the cell provided
     * via its MapCell instance. If the cell would be outside the map boundary, null is returned.
     *
     * @param position target cell in form of MapCell instance
     * @param dir      Ant instance current direction
     * @return the MapCell an ant is looking at. Null if looking out-of-bounds
     */
    public MapCell getMapCellInDir(MapCell position, Ant.Direction dir) {
        return getMapCellInDir(position.getPosition(), dir);
    }

    /**
     * Reads in the string format data file and constructs the ant world/map
     *
     * @param fileContent world file to be built
     */
    public void buildMap(String fileContent) {

        String lines[] = fileContent.split("\\r?\\n");
        int lineN = 0;
        int width = 0;
        int height = 0;
        for (String line : lines) {
            if (lineN == 0) {
                try {
                    width = Integer.parseInt(line);
                    if (width < 1) {
                        throw new IllegalArgumentException("Line " + Integer.toString(lineN+1) + ": Width is expected to" +
                                " be positive but \"" + line + "\" was found instead");
                    }
                } catch(NumberFormatException e) {
                    throw new IllegalArgumentException("Line " + Integer.toString(lineN+1) + ": Width is expected to" +
                            " be integer but \"" + line + "\" was found instead");
                }
            } else if (lineN == 1) {
                try {
                    height = Integer.parseInt(line);
                    if (height < 1) {
                        throw new IllegalArgumentException("Line " + Integer.toString(lineN+1) + ": Height is expected to" +
                                " be positive but \"" + line + "\" was found instead");
                    }
                } catch(NumberFormatException e) {
                    throw new IllegalArgumentException("Line " + Integer.toString(lineN+1) + ": Height is expected to" +
                            " be integer but \"" + line + "\" was found instead");
                }
                if (lines.length != height + 2) {
                    throw new IllegalArgumentException("Document was expected to have " + Integer.toString(height + 2) +
                            " lines but " + Integer.toString(lines.length) + " were found instead");
                }
                mapArray = new MapCell[width][height];
            } else if (lineN < height + 2) {
                String tokens[] = line.trim().split(" ");
                if (tokens.length != width){
                    throw new IllegalArgumentException("Line " + Integer.toString(lineN+1) + ": Line was expected to " +
                            "contain " + Integer.toString(width) + " tokens but " + tokens.length + " were found instead");
                }
                try {
                    for (int i = 0; i < tokens.length; ++i) {
                        mapArray[i][lineN - 2] = new MapCell(tokens[i], new Vector2i(i, lineN - 2));
                    }
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException("Line " + Integer.toString(lineN+1) + ": " + e.getMessage());
                }
            }
            ++lineN;
        }
    }

    /**
     * Gets the 2D array for the map.
     *
     * @return mapArray
     */
    public MapCell[][] getMap() {
        return mapArray;
    }

    /**
     * returns a map cell from the mapArray grid at the given coordinates
     *
     * @param x x coordinate on the map
     * @param y y coordinate on the map
     * @return MapCell
     */
    public MapCell getCell(int x, int y) {
        return getMap()[x][y];
    }


    /**
     * Returns cell on coordinates compatible with map renderer (s.t. the Y-axis is reversed. If the cell is outside of
     * the boundary, null is returned.
     * @param x X coordinate on the map
     * @param y Y coordinate on the map from bottom left corner
     * @return Cell on provided coordinates or null if outside the defined map space
     */
    public MapCell getCellBL(int x, int y) {
        if(getMap() == null) { return null; }
        if (x < 0 || x >= getMap().length || y < 0 || y >= getMap()[0].length) { return null; }
        return getMap()[x][getMap()[0].length - 1 - y];
    }

    /**
     * Indicates whether the coordinate is even for coordinate starting at bottom left (reversed Y-axis)
     * @param y Y coordinate from bottom left
     * @return True if row is even, false otherwise
     */
    public boolean isEvenBL(int y) {
        if(getMap() == null) { return (0 - 1 - y) % 2 == 1 || (0 - 1 - y) % 2 == -1; }
        return (getMap()[0].length - 1 - y) % 2 == 1 || (getMap()[0].length - 1 - y) % 2 == -1;
    }

    /**
     * Returns Height of the map
     * @return Height of the map in map cells
     */
    public int getHeight() {
        if (getMap() == null) { return 0; }
        return getMap()[0].length;
    }

    /**
     * Returns Width of the map
     * @return Width of the map in map cells
     */
    public int getWidth() {
        if (getMap() == null) { return 0; }
        return getMap().length;
    }

    /**
     * Returns the terrain of the map
     * @return Terrain of the map
     */
    public MapCell[][] getTerrain() {
        return mapArray;
    }
}
