package ant_project;

import ant_project.AntManager;
import ant_project.Map;
import ant_project.fsa.Automaton;
import junit.framework.TestCase;


/**
 * Created by Curt on 31/03/2015.
 */
public class AntManagerTest extends TestCase {

    public void testContructor() throws Exception {

        Automaton redAutomaton = new Automaton("Flip 1 0 0");
        Automaton blackAutomaton = new Automaton("Flip 1 0 0");

        Map map = new Map("10\n" +
                "10\n" +
                "# # # # # # # # # #\n" +
                " # 9 9 . . . . 3 3 #\n" +
                "# 9 # . - - - - - #\n" +
                " # . # - - - - - - #\n" +
                "# . . 5 - - - - - #\n" +
                " # + + + + + 5 . . #\n" +
                "# + + + + + + # . #\n" +
                " # + + + + + . # 9 #\n" +
                "# 3 3 . . . . 9 9 #\n" +
                " # # # # # # # # # #");

        AntManager testManager = new AntManager(redAutomaton, blackAutomaton, map);

        for (int x = 0; x < testManager.getMap().getWidth(); ++x) {
            for (int y = 0; y < testManager.getMap().getHeight(); ++y) {
                if (testManager.getMap().getCell(x, y).isAnthill(Ant.TeamColor.RED)) {
                    assertTrue(testManager.getMap().getCell(x, y).getAnt() != null);
                    assertTrue(testManager.getMap().getCell(x, y).getAnt().getColor() == Ant.TeamColor.RED);
                } else if (testManager.getMap().getCell(x, y).isAnthill(Ant.TeamColor.BLACK)) {
                    assertTrue(testManager.getMap().getCell(x, y).getAnt() != null);
                    assertTrue(testManager.getMap().getCell(x, y).getAnt().getColor() == Ant.TeamColor.BLACK);
                } else {
                    assertTrue(testManager.getMap().getCell(x, y).getAnt() == null);
                }
            }
        }

        assertEquals(testManager.getAnts().get(0).getPosition().getPosition().x, 4);
        assertEquals(testManager.getAnts().get(0).getPosition().getPosition().y, 2);

        assertEquals(testManager.getAnts().get(1).getPosition().getPosition().x, 5);
        assertEquals(testManager.getAnts().get(1).getPosition().getPosition().y, 2);

        assertEquals(testManager.getAnts().get(2).getPosition().getPosition().x, 6);
        assertEquals(testManager.getAnts().get(2).getPosition().getPosition().y, 2);

        assertEquals(testManager.getAnts().get(3).getPosition().getPosition().x, 7);
        assertEquals(testManager.getAnts().get(3).getPosition().getPosition().y, 2);

        assertEquals(testManager.getAnts().get(4).getPosition().getPosition().x, 8);
        assertEquals(testManager.getAnts().get(4).getPosition().getPosition().y, 2);

        assertEquals(testManager.getAnts().get(5).getPosition().getPosition().x, 3);
        assertEquals(testManager.getAnts().get(5).getPosition().getPosition().y, 3);

        assertEquals(testManager.getAnts().get(6).getPosition().getPosition().x, 4);
        assertEquals(testManager.getAnts().get(6).getPosition().getPosition().y, 3);

        assertEquals(testManager.getAnts().get(7).getPosition().getPosition().x, 5);
        assertEquals(testManager.getAnts().get(7).getPosition().getPosition().y, 3);

        assertEquals(testManager.getAnts().get(8).getPosition().getPosition().x, 6);
        assertEquals(testManager.getAnts().get(8).getPosition().getPosition().y, 3);

        assertEquals(testManager.getAnts().get(9).getPosition().getPosition().x, 7);
        assertEquals(testManager.getAnts().get(9).getPosition().getPosition().y, 3);

        assertEquals(testManager.getAnts().get(10).getPosition().getPosition().x, 8);
        assertEquals(testManager.getAnts().get(10).getPosition().getPosition().y, 3);

        assertEquals(testManager.getAnts().get(11).getPosition().getPosition().x, 4);
        assertEquals(testManager.getAnts().get(11).getPosition().getPosition().y, 4);

        assertEquals(testManager.getAnts().get(12).getPosition().getPosition().x, 5);
        assertEquals(testManager.getAnts().get(12).getPosition().getPosition().y, 4);

        assertEquals(testManager.getAnts().get(13).getPosition().getPosition().x, 6);
        assertEquals(testManager.getAnts().get(13).getPosition().getPosition().y, 4);

        assertEquals(testManager.getAnts().get(14).getPosition().getPosition().x, 7);
        assertEquals(testManager.getAnts().get(14).getPosition().getPosition().y, 4);

        assertEquals(testManager.getAnts().get(15).getPosition().getPosition().x, 8);
        assertEquals(testManager.getAnts().get(15).getPosition().getPosition().y, 4);

        assertEquals(testManager.getAnts().get(16).getPosition().getPosition().x, 1);
        assertEquals(testManager.getAnts().get(16).getPosition().getPosition().y, 5);

        assertEquals(testManager.getAnts().get(17).getPosition().getPosition().x, 2);
        assertEquals(testManager.getAnts().get(17).getPosition().getPosition().y, 5);

        assertEquals(testManager.getAnts().get(18).getPosition().getPosition().x, 3);
        assertEquals(testManager.getAnts().get(18).getPosition().getPosition().y, 5);

        assertEquals(testManager.getAnts().get(19).getPosition().getPosition().x, 4);
        assertEquals(testManager.getAnts().get(19).getPosition().getPosition().y, 5);

        assertEquals(testManager.getAnts().get(20).getPosition().getPosition().x, 5);
        assertEquals(testManager.getAnts().get(20).getPosition().getPosition().y, 5);

        assertEquals(testManager.getAnts().get(21).getPosition().getPosition().x, 1);
        assertEquals(testManager.getAnts().get(21).getPosition().getPosition().y, 6);

        assertEquals(testManager.getAnts().get(22).getPosition().getPosition().x, 2);
        assertEquals(testManager.getAnts().get(22).getPosition().getPosition().y, 6);

        assertEquals(testManager.getAnts().get(23).getPosition().getPosition().x, 3);
        assertEquals(testManager.getAnts().get(23).getPosition().getPosition().y, 6);

        assertEquals(testManager.getAnts().get(24).getPosition().getPosition().x, 4);
        assertEquals(testManager.getAnts().get(24).getPosition().getPosition().y, 6);

        assertEquals(testManager.getAnts().get(25).getPosition().getPosition().x, 5);
        assertEquals(testManager.getAnts().get(25).getPosition().getPosition().y, 6);

        assertEquals(testManager.getAnts().get(26).getPosition().getPosition().x, 6);
        assertEquals(testManager.getAnts().get(26).getPosition().getPosition().y, 6);

        assertEquals(testManager.getAnts().get(27).getPosition().getPosition().x, 1);
        assertEquals(testManager.getAnts().get(27).getPosition().getPosition().y, 7);

        assertEquals(testManager.getAnts().get(28).getPosition().getPosition().x, 2);
        assertEquals(testManager.getAnts().get(28).getPosition().getPosition().y, 7);

        assertEquals(testManager.getAnts().get(29).getPosition().getPosition().x, 3);
        assertEquals(testManager.getAnts().get(29).getPosition().getPosition().y, 7);

        assertEquals(testManager.getAnts().get(30).getPosition().getPosition().x, 4);
        assertEquals(testManager.getAnts().get(30).getPosition().getPosition().y, 7);

        assertEquals(testManager.getAnts().get(31).getPosition().getPosition().x, 5);
        assertEquals(testManager.getAnts().get(31).getPosition().getPosition().y, 7);

    }
}