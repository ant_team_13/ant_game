package ant_project;

/**
 * Check whether or not the map is valid for tournament application
 */
public class MapChecker {
    private static final int WIDTH = 150;
    private static final int HEIGHT = 150;
    private static final int NUM_ROCKS = 14;
    private static final int ANTHILL_SIDE = 7;
    private static final int FOOD_SIDE = 5;
    private static final int FOOD_AMOUNT = 5;
    private static final int BLOBS_AMOUNT = 11;

    /**
     * Check the map provided. Throws exception if there is issue, returns gracefully otherwise.
     * @param map Map to be checked
     */
    public static void check(Map map) {
        int width = map.getWidth();
        int height = map.getHeight();

        if (width != WIDTH || height != HEIGHT) {
            throw new IllegalArgumentException("Dimensions of map expected to be [" + Integer.toString(WIDTH)
                    + ", " + Integer.toString(HEIGHT) + "], instead found [" + Integer.toString(width)
                    + ", " + Integer.toString(height) + "]");
        }

        MapCell[][] terrain = map.getTerrain();

        //Initialize and check frame
        boolean checked[] = new boolean[map.getWidth() * map.getHeight()];
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                if (x == 0 || y == 0 || x == width - 1 || y == height - 1) {
                    if (terrain[x][y].isRocky() != true) {
                        throw new IllegalArgumentException("Cell [" + Integer.toString(x)
                                + ", " + Integer.toString(y) + "] expected to be rocky.");
                    }
                    checked[y * width + x] = true;
                } else if (x == 1 || y == 1 || x == width - 2 || y == height - 2) {
                    if (!terrain[x][y].isClear()) {
                        throw new IllegalArgumentException("Cell [" + Integer.toString(x)
                                + ", " + Integer.toString(y) + "] expected to be clear.");
                    }
                    checked[y * width + x] = true;
                } else {
                    checked[y * width + x] = false;
                }
            }
        }

        //Check rocks
        int rockCount = 0;
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                if (!checked[y * width + x] && terrain[x][y].isRocky()) {
                    ++rockCount;
                    if (terrain[x + 1][y].isClear()) {
                        checked[y * map.getWidth() + (x + 1)] = true;
                    } else {
                        throw new IllegalArgumentException("Cell [" + Integer.toString(x + 1)
                                + ", " + Integer.toString(y) + "] expected to be clear.");
                    }
                    if (terrain[x - 1][y].isClear()) {
                        checked[y * map.getWidth() + (x - 1)] = true;
                    } else {
                        throw new IllegalArgumentException("Cell [" + Integer.toString(x - 1)
                                + ", " + Integer.toString(y) + "] expected to be clear.");
                    }


                    int xOffset = 0;
                    if (y % 2 == 1) {
                        ++xOffset;
                    }
                    for (int xn = -1; xn < 1; ++xn) {
                        if (terrain[xn + xOffset + x][y + 1].isClear()) {
                            checked[(y + 1) * map.getWidth() + xn + xOffset + x] = true;
                        } else {
                            throw new IllegalArgumentException("Cell [" + Integer.toString(xn + xOffset + x)
                                    + ", " + Integer.toString(y + 1) + "] expected to be clear.");
                        }
                        if (terrain[xn + xOffset + x][y - 1].isClear()) {
                            checked[(y - 1) * map.getWidth() + xn + xOffset + x] = true;
                        } else {
                            throw new IllegalArgumentException("Cell [" + Integer.toString(xn + xOffset + x)
                                    + ", " + Integer.toString(y - 1) + "] expected to be clear.");
                        }
                    }
                }
            }
        }
        if (rockCount != NUM_ROCKS) {
            throw new IllegalArgumentException("Map expected to contain" + Integer.toString(NUM_ROCKS) + ", "
                    + Integer.toString(rockCount) + "found instead.");
        }

        //Check anthills
        checkAnthill(terrain, width, height, checked, Ant.TeamColor.BLACK);
        checkAnthill(terrain, width, height, checked, Ant.TeamColor.RED);

        //Check food separately since map format does not allow for non-clear food cells.
        boolean checkedFood[] = new boolean[map.getWidth() * map.getHeight()];
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                checkedFood[y * width + x] = false;
            }
        }
        int foodCount = 0;
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                if (!checkedFood[y * width + x] && terrain[x][y].getFoodAmount() > 0)
                {
                    if (checkFoodBlobC(terrain,width, height, checkedFood, x, y)) {
                        ++foodCount;
                        continue;
                    }
                    if (checkFoodBlobA(terrain,width, height, checkedFood, x, y)) {
                        ++foodCount;
                        continue;
                    }
                    if (checkFoodBlobB(terrain,width, height, checkedFood, x, y)) {
                        ++foodCount;
                        continue;
                    }
                    if (!checkedFood[(y+1) * width + x] && terrain[x][y+1].getFoodAmount() > 0) {
                        if (checkFoodBlobC(terrain,width, height, checkedFood, x, y + 1)) {
                            ++foodCount;
                            continue;
                        }
                    }
                    throw new IllegalArgumentException("Food Blob on cell [" + Integer.toString(x)
                        + ", " + Integer.toString(y) + "] is invalid.");
                }
            }
        }
        if (foodCount != BLOBS_AMOUNT) {
            throw new IllegalArgumentException("Map expected to contain" + Integer.toString(BLOBS_AMOUNT) + ", "
                    + Integer.toString(foodCount) + "found instead.");
        }
    }

    /**
     * Checks whether anthill on specified position is valid
     * @param terrain Terrain of the map
     * @param width Width of the map
     * @param height Height of the map
     * @param checked Array of checked cells
     * @param color Color of the anthill
     */
    private static void checkAnthill(MapCell[][] terrain, int width, int height, boolean[] checked, Ant.TeamColor color) {
        for (int coordX = 0; coordX < width; ++coordX) {
            for (int coordY = 0; coordY < height; ++coordY) {
                if (!checked[coordY * width + coordX] && terrain[coordX][coordY].isAnthill(color)) {
                    if (!checked[(coordY+1) * width + coordX] && terrain[coordX][coordY+1].isAnthill(color)) {
                        ++coordY;
                    }
                    for (int x = -1; x <= (ANTHILL_SIDE - 1) * 2 + 1; ++x) {
                        if (x >= 0 && x < (ANTHILL_SIDE - 1) * 2 + 1) {
                            if (terrain[x + coordX][coordY].isAnthill(color)) {
                                checked[coordY * width + x + coordX] = true;
                            } else {
                                throw new IllegalArgumentException("Cell [" + Integer.toString(x + coordX)
                                        + ", " + Integer.toString(coordY) + "] expected to be anthill.");
                            }
                        }
                        else {
                            if (terrain[x + coordX][coordY].isClear()) {
                                checked[coordY * width + x + coordX] = true;
                            } else {
                                throw new IllegalArgumentException("Cell [" + Integer.toString(x + coordX)
                                        + ", " + Integer.toString(coordY) + "] expected to be clear.");
                            }
                        }
                    }

                    int xOffset = 0;
                    for (int y = 1; y <= ANTHILL_SIDE; ++y) {
                        if ((coordY + y) % 2 == 0) {
                            ++xOffset;
                        }

                        for (int x = -1; x <= (ANTHILL_SIDE - 1) * 2 - y + 1; ++x) {
                            if (x >= 0 && x < (ANTHILL_SIDE - 1) * 2 - y + 1 && y < ANTHILL_SIDE) {
                                if (terrain[x + coordX + xOffset][coordY + y].isAnthill(color)) {
                                    checked[(coordY + y) * width + x + coordX + xOffset] = true;
                                } else {
                                    throw new IllegalArgumentException("Cell [" + Integer.toString(x + coordX + xOffset)
                                            + ", " + Integer.toString((coordY + y)) + "] expected to be anthill.");
                                }
                                if (terrain[x + coordX + xOffset][coordY - y].isAnthill(color)) {
                                    checked[(coordY - y) * width + x + coordX + xOffset] = true;
                                } else {
                                    throw new IllegalArgumentException("Cell [" + Integer.toString(x + coordX + xOffset)
                                            + ", " + Integer.toString((coordY - y)) + "] expected to be anthill.");
                                }
                            } else {
                                if (terrain[x + coordX + xOffset][coordY + y].isClear()) {
                                    checked[(coordY + y) * width + x + coordX + xOffset] = true;
                                } else {
                                    throw new IllegalArgumentException("Cell [" + Integer.toString(x + coordX + xOffset)
                                            + ", " + Integer.toString((coordY + y)) + "] expected to be clear.");
                                }
                                if (terrain[x + coordX + xOffset][coordY - y].isClear()) {
                                    checked[(coordY - y) * width + x + coordX + xOffset] = true;
                                } else {
                                    throw new IllegalArgumentException("Cell [" + Integer.toString(x + coordX + xOffset)
                                            + ", " + Integer.toString((coordY - y)) + "] expected to be clear.");
                                }
                            }
                        }
                    }
                    return;
                }
            }
        }
    }

    /**
     *  Tries to fit Food Blob Type A (No slant) to current position
     * @param terrain Terrain of the map
     * @param width Width of the map
     * @param height Height of the map
     * @param checked Array of checked cells
     * @param coordX Position of the food blob
     * @param coordY Position of the food blob
     * @return True if the food blob fits, false otherwise
     */
    private static boolean checkFoodBlobA(MapCell[][] terrain, int width, int height, boolean[] checked, int coordX, int coordY) {
        if (coordX + FOOD_SIDE > width || coordY + FOOD_SIDE > height) {
            return false;
        }
        for (int y = 0; y < FOOD_SIDE; ++y) {
            for (int x = 0; x < FOOD_SIDE; ++x) {
                if(checked[(coordY + y) * width + x + coordX]
                        || terrain[x + coordX][coordY + y].getFoodAmount() != FOOD_AMOUNT) {
                    return false;
                }
            }
        }
        for (int y = 0; y < FOOD_SIDE; ++y) {
            for (int x = 0; x < FOOD_SIDE; ++x) {
                checked[(coordY + y) * width + x + coordX] = true;
            }
        }
        return true;
    }

    /**
     *  Tries to fit Food Blob Type B (slant 1) to current position
     * @param terrain Terrain of the map
     * @param width Width of the map
     * @param height Height of the map
     * @param checked Array of checked cells
     * @param coordX Position of the food blob
     * @param coordY Position of the food blob
     * @return True if the food blob fits, false otherwise
     */
    private static boolean checkFoodBlobB(MapCell[][] terrain, int width, int height, boolean[] checked, int coordX, int coordY) {
        int xOffset = 0;
        for (int y = 1; y <= FOOD_SIDE - 1; ++y) {
            if ((FOOD_SIDE + y) % 2 == 0) {  ++xOffset; }
        }

        if (coordX + FOOD_SIDE + xOffset > width || coordY + FOOD_SIDE > height) {
            return false;
        }

        xOffset = 0;
        for (int y = 0; y < FOOD_SIDE; ++y) {
            if ((coordY + y) % 2 == 0 && y != 0) {  ++xOffset; }
            for (int x = 0; x < FOOD_SIDE; ++x) {
                if(checked[(coordY + y) * width + x + coordX + xOffset]
                        || terrain[x + coordX + xOffset][coordY + y].getFoodAmount() != FOOD_AMOUNT) {
                    return false;
                }
            }
        }

        xOffset = 0;
        for (int y = 0; y < FOOD_SIDE; ++y) {
            if ((coordY + y) % 2 == 0 && y != 0) {  ++xOffset; }
            for (int x = 0; x < FOOD_SIDE; ++x) {
                checked[(coordY + y) * width + x + coordX + xOffset] = true;
            }
        }
        return true;
    }

    /**
     *  Tries to fit Food Blob Type B (slant 2) to current position
     * @param terrain Terrain of the map
     * @param width Width of the map
     * @param height Height of the map
     * @param checked Array of checked cells
     * @param coordX Position of the food blob
     * @param coordY Position of the food blob
     * @return True if the food blob fits, false otherwise
     */
    private static boolean checkFoodBlobC(MapCell[][] terrain, int width, int height, boolean[] checked, int coordX, int coordY) {
        int xOffset = 0;
        for (int y = 1; y <= FOOD_SIDE - 1; ++y) {
            if ((FOOD_SIDE + y) % 2 == 0) {  ++xOffset; }
        }

        if (coordX + FOOD_SIDE + xOffset > width || coordY - FOOD_SIDE < -1) {
            return false;
        }

        xOffset = 0;
        for (int y = 0; y < FOOD_SIDE; ++y) {
            if ((coordY + y) % 2 == 0 && y != 0) {  ++xOffset; }
            for (int x = 0; x < FOOD_SIDE; ++x) {
                if(checked[(coordY - y) * width + x + coordX + xOffset]
                        || terrain[x + coordX + xOffset][coordY - y].getFoodAmount() != FOOD_AMOUNT) {
                    return false;
                }
            }
        }

        xOffset = 0;
        for (int y = 0; y < FOOD_SIDE; ++y) {
            if ((coordY + y) % 2 == 0 && y != 0) {  ++xOffset; }
            for (int x = 0; x < FOOD_SIDE; ++x) {
                checked[(coordY - y) * width + x + coordX + xOffset] = true;
            }
        }
        return true;
    }
}
