package ant_project.applications;

import ant_project.Map;
import ant_project.Simulation;
import ant_project.SimulationResults;
import ant_project.fsa.Automaton;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Daniel on 06/02/2015.
 */

/**
 * Provides means of bulk matching of brains for purposes of development
 */
public class FastBulkSimulator {
    private int numBrains;
    private int numMaps;
    private int rematchCount;
    private boolean verbose;
    HashMap<String,Automaton> automatons;
    HashMap<String, String> maps;
    HashMap<String, Integer> automatonWins;

    public static void main(String[] args) {
        (new FastBulkSimulator()).run();
    }

    /**
     * Zero-initializes the simulator
     */
    FastBulkSimulator() {
        numBrains = 0;
        numMaps = 0;
        automatons = new HashMap<String, Automaton>();
        automatonWins = new HashMap<String, Integer>();
        maps = new HashMap<String, String>();
        verbose = false;
    }

    /**
     * Runs the simulation wizard
     */
    void run() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        {
            File folder = new File("data/brains");
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {

                    String content = "";
                    try {
                        content = new String(Files.readAllBytes(Paths.get(listOfFiles[i].getAbsolutePath())));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    automatons.put(listOfFiles[i].getName(), new Automaton(content));
                    automatonWins.put(listOfFiles[i].getName(), 0);
                    ++numBrains;
                }
            }
            System.out.println("Ant brains loaded. Total number of brains is " + numBrains + ".");
        }
        {
            File folder = new File("data/world");
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    maps.put(listOfFiles[i].getName(), listOfFiles[i].getAbsolutePath());
                    ++numMaps;
                }
            }
            System.out.println("Ant World Maps loaded. Total number of maps is " + numMaps + ".");
        }

        System.out.print("Please enter number of rematches. Default is 5:");
        String input1 = null;
        try {
            input1 = bufferRead.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            rematchCount = Integer.parseInt(input1);
        } catch(NumberFormatException e) {
            rematchCount = 5;
            System.out.println("Defaulting to 5.");
        }

        System.out.print("Enable verbose?y/N");
        String input2 = null;
        try {
            input2 = bufferRead.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (input2.toLowerCase().equals("y")) {
            System.out.println("Verbose enabled");
            verbose = true;
        } else {
            verbose = false;
            System.out.println("Verbose disabled");
        }

        int experiment = 0;
        for (java.util.Map.Entry<String, String> m : maps.entrySet()) {
            String content = "";
            try {
                content = new String(Files.readAllBytes(Paths.get(m.getValue())));
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (java.util.Map.Entry<String, Automaton> a1 : automatons.entrySet()) {
                for (java.util.Map.Entry<String, Automaton> a2 : automatons.entrySet()) {
                    if (a1.getKey() == a2.getKey()) { continue; }
                    for (int retry = 0; retry < rematchCount; ++retry) {
                        ++experiment;
                        if (verbose) {
                            System.out.println("-------------------------------------------------------------");
                        }
                        System.out.print("Match " + Integer.toString(experiment) + " out of " + Integer.toString(rematchCount * automatons.size() * (automatons.size() - 1) * maps.size()) + " ");
                        if (verbose) {
                            System.out.println("\nMap: " + m.getKey());
                            System.out.println("Red Automaton: " + a1.getKey());
                            System.out.println("Black Automaton : " + a2.getKey());
                        }
                        Map map = new Map(content);
                        Simulation s = new Simulation(map, a1.getValue(), a2.getValue()) {
                            @Override
                            protected void progressReport() {
                                if (currentTick % 10000 == 0) { System.out.print("#"); }
                            }
                        };
                        SimulationResults sr = s.run();
                        if (sr.result == SimulationResults.Result.RED_WINS) {
                            automatonWins.put(a1.getKey(),automatonWins.get(a1.getKey()) + 1);
                        }
                        else if (sr.result == SimulationResults.Result.BLACK_WINS) {
                            automatonWins.put(a2.getKey(),automatonWins.get(a2.getKey()) + 1);
                        }
                        System.out.print("\n");
                        if (verbose) {
                            System.out.println("STATS");
                            System.out.println("Red Automaton - Ants left: " + Integer.toString(sr.redAnts));
                            System.out.println("Red Automaton - Food collected: " + Integer.toString(sr.redCollected));
                            System.out.println("Black Automaton - Ants left: " + Integer.toString(sr.blackAnts));
                            System.out.println("Black Automaton - Food collected: " + Integer.toString(sr.blackCollected));
                            System.out.println("RESULT: " + sr.result.toString());
                        }
                    }
                }
            }
        }

        System.out.println("EXPERIMENT RESULTS");
        for (java.util.Map.Entry<String, Integer> res : automatonWins.entrySet()) {
            System.out.println("Wins of " + res.getKey() + ": " + res.getValue().toString());
        }
    }
}
