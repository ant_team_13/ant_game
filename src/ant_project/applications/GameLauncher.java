package ant_project.applications;

import com.sun.javafx.binding.MapExpressionHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Class implementing the Game Launcher
 */
public class GameLauncher {
    private JButton exitButtion;
    private JButton generateButton;
    private JButton mapValidateButton;
    private JButton scriptValidateButton;
    private JButton hostTournamentButton;
    private JButton newGameButton;
    private JPanel mainPanel;
    private JFrame frame;


    public GameLauncher() {
        newGameButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.setVisible(false);
                GameSimulator gs = new GameSimulator();
                gs.getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                gs.getFrame().addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frame.setVisible(true);
                    }
                });
            }
        });

        hostTournamentButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.setVisible(false);
                TournamentHosting th = new TournamentHosting();
                th.getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                th.getFrame().addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frame.setVisible(true);
                    }
                });
            }
        });

        scriptValidateButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.setVisible(false);
                BrainValidator bv = new BrainValidator();
                bv.getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                bv.getFrame().addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frame.setVisible(true);
                    }
                });
            }
        });

        mapValidateButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.setVisible(false);
                MapValidator mv = new MapValidator();
                mv.getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                mv.getFrame().addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frame.setVisible(true);
                    }
                });
            }
        });

        generateButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.setVisible(false);
                MapGenerator mg = new MapGenerator();
                mg.getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                mg.getFrame().addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frame.setVisible(true);
                    }
                });

            }
        });

        exitButtion.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }
        });

        frame = new JFrame("Game Launcher");

        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Dimension dimMinimum = frame.getMinimumSize();
        Dimension dimMaximum = frame.getMaximumSize();
        dimMinimum.setSize(700, 65);
        dimMaximum.setSize(2000, 65);
        frame.setMinimumSize(dimMinimum);
        frame.setMaximumSize(dimMaximum);
        frame.setLocationRelativeTo( null );
        frame.setResizable(false);
        frame.setVisible(true);
    }
}
