package ant_project.applications;

import ant_project.Tournament;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by Daniel on 10/04/2015.
 */
public class TournamentHosting {
    private JTextField brainFolderField;
    private JTextField mapFolderField;
    private JButton brainBrowseButton;
    private JButton mapBrowseButton;
    private JPanel mainPanel;
    private JButton runButton;
    private JProgressBar gameProgressBar;
    private JProgressBar tournamentProgressBar;
    private JLabel thirdPlace;
    private JLabel secondPlace;
    private JLabel firstPlace;
    private JFrame frame;

    public TournamentHosting() {
        brainBrowseButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setCurrentDirectory(new File("."));
                int result = fileChooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    brainFolderField.setText(selectedFile.getAbsolutePath());
                }
            }
        });
        mapBrowseButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setCurrentDirectory(new File("."));
                int result = fileChooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    mapFolderField.setText(selectedFile.getAbsolutePath());
                }
            }
        });

        runButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                SwingWorker sim = new SwingWorker<Void,Void>() {
                    public Void doInBackground() {
                        try {
                            runButton.setEnabled(false);
                            Tournament t = new Tournament(mapFolderField.getText(), brainFolderField.getText()) {
                                @Override
                                protected void gameUpdate() {
                                    gameProgressBar.setValue(this.gameProgress);
                                    tournamentProgressBar.setValue(this.gamesDone);
                                    tournamentProgressBar.setMaximum(this.gamesTotal);
                                    tournamentProgressBar.setString(Integer.toString(this.gamesDone) + "/" + Integer.toString(this.gamesTotal));
                                }
                            };
                            t.runTournament();
                            String[] winners = t.getWinners();
                            firstPlace.setText(winners[0]);
                            secondPlace.setText(winners[1]);
                            thirdPlace.setText(winners[2]);
                        } catch (Exception ex) {
                            showMessageDialog(null, "Problem occurred during Tournament: " + ex.getMessage());
                        }
                        runButton.setEnabled(true);
                        return null;
                    }
                };
                sim.execute();
            }
        });

        frame = new JFrame("Tournament Hosting");

        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Dimension dimMinimum = frame.getMinimumSize();
        Dimension dimMaximum = frame.getMaximumSize();
        dimMinimum.setSize(700, 65);
        dimMaximum.setSize(2000, 65);
        frame.setMinimumSize(dimMinimum);
        frame.setMaximumSize(dimMaximum);
        frame.setLocationRelativeTo( null );
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Returns Main JFrame
     * @return Main JFrame
     */
    public JFrame getFrame() {
        return frame;
    }
}
