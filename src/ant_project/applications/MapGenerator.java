package ant_project.applications;

import ant_project.renderer.MapCanvas;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl.LwjglCanvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Class implementing the application for random map generation
 */
public class MapGenerator {
    private JFrame frame;
    private JFrame frameDisplay;
    private Thread startSlave;

    private LwjglCanvas canvas;


    /**
     * Initializes the map generator
     */
    public MapGenerator() {
        super();
        frame = new JFrame("Map Generator");
        frameDisplay = new JFrame("Map Generator");


        frame.addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                frame.dispose();
                LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
                config.title = "Map Generator";
                config.width = 1024;
                config.height = 768;
                config.backgroundFPS = 200;
                config.foregroundFPS = 200;
                config.vSyncEnabled = false;
                final MapCanvas mc = new MapCanvas();
                canvas = new LwjglCanvas(mc, config);

                Container container = frameDisplay.getContentPane();
                container.add(canvas.getCanvas(), BorderLayout.CENTER);
                container.add(new MapGeneratorPropPanel(mc).mapGeneratorPropPanel, BorderLayout.WEST);
                frameDisplay.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frame.dispose();
                    }});
                frameDisplay.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frameDisplay.pack();
                Dimension dimMinimumF = frameDisplay.getMinimumSize();
                Dimension dimMaximumF = frameDisplay.getMaximumSize();
                dimMinimumF.setSize(1024, 768);
                dimMaximumF.setSize(1024, 768);
                frameDisplay.setMinimumSize(dimMinimumF);
                frameDisplay.setMaximumSize(dimMaximumF);
                frameDisplay.setLocationRelativeTo( null );
                frameDisplay.setVisible(true);
            }
        });

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Dimension dimMinimum = frame.getMinimumSize();
        Dimension dimMaximum = frame.getMaximumSize();
        dimMinimum.setSize(700, 75);
        dimMaximum.setSize(2000, 75);
        frame.setMinimumSize(dimMinimum);
        frame.setMaximumSize(dimMaximum);
        frame.setLocationRelativeTo( null );
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setVisible(false);
    }

    /**
     * Returns Main JFrame
     * @return Main JFrame
     */
    public JFrame getFrame() {
        return frameDisplay;
    }
}
