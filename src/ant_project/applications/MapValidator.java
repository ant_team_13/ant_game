package ant_project.applications;

import ant_project.Map;
import ant_project.MapChecker;
import ant_project.fsa.Automaton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Class implementing the application for map validation
 */
public class MapValidator {
    private JTextField mapPathField;
    private JPanel mainPanel;
    private JButton browseForMapButton;
    private JButton validateMapButton;
    private JFrame frame;

    /**
     * Initialize the map validator
     */
    public MapValidator() {
        browseForMapButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("."));
                int result = fileChooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    mapPathField.setText(selectedFile.getAbsolutePath());
                }
            }
        });
        validateMapButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                String content = "";
                try {
                    content = new String(Files.readAllBytes(Paths.get(mapPathField.getText())));
                } catch (IOException e) {
                    showMessageDialog(null, "Error occurred: File was not found or could not be accessed.");
                    return;
                }
                Map m;
                try {
                    m = new Map(content);
                } catch (Exception e) {
                    showMessageDialog(null, "Error during map validation: " + e.getMessage());
                    return;
                }
                try {
                    MapChecker.check(m);
                } catch (Exception e) {
                    showMessageDialog(null, "Map file format is valid but map does not satisfy tournament conditions: " + e.getMessage());
                    return;
                }
                showMessageDialog(null, "Success. Map file is valid and ready for tournaments.");
            }
        });

        frame = new JFrame("Map Validator");

        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Dimension dimMinimum = frame.getMinimumSize();
        Dimension dimMaximum = frame.getMaximumSize();
        dimMinimum.setSize(700, 65);
        dimMaximum.setSize(2000, 65);
        frame.setMinimumSize(dimMinimum);
        frame.setMaximumSize(dimMaximum);
        frame.setLocationRelativeTo( null );
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Returns Main JFrame
     * @return Main JFrame
     */
    public JFrame getFrame() {
        return frame;
    }
}
