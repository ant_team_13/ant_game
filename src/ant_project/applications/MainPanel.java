package ant_project.applications;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;


/**
 * Class implementing the UI for Game file choosing
 */
public class MainPanel {
    private JButton browseForRedAntButton;
    private JButton browseForBlackAntButton;
    private JButton browseForAntWorldButton;
    JPanel mainPanel;
    private JTextField redBrainPathField;
    private JTextField blackBrainPathField;
    private JTextField antWorldPathField;
    JButton runGameWithGraphicsButton;
    JButton runGameFastButton;

    /**
     * Initializes the main panel
     */
    public MainPanel() {
        browseForRedAntButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("."));
                int result = fileChooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    redBrainPathField.setText(selectedFile.getAbsolutePath());
                }
            }
        });
        browseForBlackAntButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("."));
                int result = fileChooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    blackBrainPathField.setText(selectedFile.getAbsolutePath());
                }
            }
        });
        browseForAntWorldButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("."));
                int result = fileChooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    antWorldPathField.setText(selectedFile.getAbsolutePath());
                }
            }
        });
    }

    /**
     * Gets the path of red team automaton file
     * @return Path to red team automaton file
     */
    public String getRedBrainPath() {
        return redBrainPathField.getText();
    }

    /**
     * Gets the path of black team automaton file
     * @return Path to black team automaton file
     */
    public String getBlackBrainPath() {
        return blackBrainPathField.getText();
    }

    /**
     * Gets the path of game map
     * @return Path to game map
     */
    public String getMapPath() {
        return antWorldPathField.getText();
    }
}
