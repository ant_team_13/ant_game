package ant_project.applications;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

/**
 * Class implementing UI for selecting map for map visualization
 */
public class MapChooserPanel {
    private JTextField mapPathField;
    private JButton browseForMapButton;
    public JButton displayMapButton;
    public JPanel mainPanel;

    /**
     * Initialize the map chooser
     */
    public MapChooserPanel() {
        browseForMapButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("."));
                int result = fileChooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    mapPathField.setText(selectedFile.getAbsolutePath());
                }
            }
        });
    }

    /**
     * Gets the path of game map
     * @return Path to game map
     */
    public String getMapPath() {
        return mapPathField.getText();
    }
}
