package ant_project.applications;

import ant_project.Map;
import ant_project.MapChecker;
import ant_project.fsa.Automaton;
import ant_project.renderer.CameraView;
import ant_project.renderer.GameCanvas;
import ant_project.renderer.MapRenderer;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl.LwjglCanvas;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by Daniel on 06/02/2015.
 */

/**
 * Class implementing application for game simulation
 */
public class GameSimulator {
    private SpriteBatch batch;
    private BitmapFont font;
    private MapRenderer mr;
    private CameraView cv;
    private Viewport viewport;
    private Map m;
    private OrthographicCamera camera;
    private String mapFile;

    private JFrame frame;
    private JFrame frameFast;

    private LwjglCanvas canvas;
    private JFrame frameSlow;

    /**
     * Creates the game simulator
     */
    public GameSimulator()
    {
        super();
        mapFile = "";

        //Intro
        final MainPanel mp = new MainPanel();
        frame = new JFrame("Game Simulator");

        mp.runGameWithGraphicsButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                {
                    String content = "";
                    try {
                        content = new String(Files.readAllBytes(Paths.get(mp.getRedBrainPath())));
                    } catch (IOException ex) {
                        showMessageDialog(null, "Error occurred: File \"" + mp.getRedBrainPath() + "\" was not found or could not be accessed.");
                        return;
                    }
                    try {
                        new Automaton(content);
                    } catch (Exception ex) {
                        showMessageDialog(null, "Error during script validation: " + ex.getMessage());
                        return;
                    }
                }

                {
                    String content = "";
                    try {
                        content = new String(Files.readAllBytes(Paths.get(mp.getBlackBrainPath())));
                    } catch (IOException ex) {
                        showMessageDialog(null, "Error occurred: File \"" + mp.getBlackBrainPath() + "\" was not found or could not be accessed.");
                        return;
                    }
                    try {
                        new Automaton(content);
                    } catch (Exception ex) {
                        showMessageDialog(null, "Error during script validation: " + ex.getMessage());
                        return;
                    }
                }

                {
                    String content = "";
                    try {
                        content = new String(Files.readAllBytes(Paths.get(mp.getMapPath())));
                    } catch (IOException ex) {
                        showMessageDialog(null, "Error occurred: File was not found or could not be accessed.");
                        return;
                    }
                    Map m;
                    try {
                        m = new Map(content);
                    } catch (Exception ex) {
                        showMessageDialog(null, "Error during map validation: " + ex.getMessage());
                        return;
                    }
                }

                frame.setVisible(false);
                LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
                config.title = "Map Display";
                config.width = 1024;
                config.height = 768;
                config.backgroundFPS = 144;
                config.foregroundFPS = 144;
                config.vSyncEnabled = false;
                final GameCanvas mc = new GameCanvas(mp.getRedBrainPath(), mp.getBlackBrainPath(), mp.getMapPath());
                canvas = new LwjglCanvas(mc, config);

                frameSlow = new JFrame("Map Display");
                frameSlow.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frame.setVisible(true);
                    }});
                frameSlow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                Container container = frameSlow.getContentPane();
                container.add(canvas.getCanvas(), BorderLayout.CENTER);
                container.add(new GameDisplayPanel(mc, frame).gameDisplayPanel,BorderLayout.SOUTH);
                frameSlow.pack();
                Dimension dimMinimumF = frameSlow.getMinimumSize();
                Dimension dimMaximumF = frameSlow.getMaximumSize();
                dimMinimumF.setSize(1024, 768);
                dimMaximumF.setSize(1024, 768);
                frameSlow.setMinimumSize(dimMinimumF);
                frameSlow.setMaximumSize(dimMaximumF);
                frameSlow.setLocationRelativeTo( null );
                frameSlow.setVisible(true);
            }
        });
        mp.runGameFastButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.setVisible(false);
                //Fast
                FastPanel fp = new FastPanel(mp.getRedBrainPath(), mp.getBlackBrainPath(), mp.getMapPath());
                frameFast = new JFrame("Fast Game Simulation");
                frameFast.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frameFast.setVisible(false);
                        frame.setVisible(true);
                    }});
                frameFast.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frameFast.setContentPane(fp.fastPanel);
                frameFast.pack();
                Dimension dimMinimumF = frameFast.getMinimumSize();
                Dimension dimMaximumF = frameFast.getMaximumSize();
                dimMinimumF.setSize(700, 205);
                dimMaximumF.setSize(2000, 205);
                frameFast.setMinimumSize(dimMinimumF);
                frameFast.setMaximumSize(dimMaximumF);
                frameFast.setLocationRelativeTo( null );
                frameFast.setResizable(false);
                frameFast.setVisible(true);
            }
        });

        frame.setContentPane(mp.mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Dimension dimMinimum = frame.getMinimumSize();
        Dimension dimMaximum = frame.getMaximumSize();
        dimMinimum.setSize(700, 205);
        dimMaximum.setSize(2000, 205);
        frame.setMinimumSize(dimMinimum);
        frame.setMaximumSize(dimMaximum);
        frame.setLocationRelativeTo( null );
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Returns Main JFrame
     * @return Main JFrame
     */
    public JFrame getFrame() {
        return frame;
    }
}