package ant_project.applications;

import ant_project.Simulation;
import ant_project.SimulationResults;

import javax.swing.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Class implementing UI for Fast Simulation Panel
 */
public class FastPanel {
    JPanel fastPanel;
    private JProgressBar progressBar;
    private JLabel redFoodLabel;
    private JLabel redAliveLabel;
    private JLabel blackFoodLabel;
    private JLabel blackAntsAlive;
    private JLabel winnerLabel;
    private SwingWorker<Void,Void> sim;


    /**
     * Constructs the panel using automaton and map files
     * @param redBrainFile File for Red Team Automaton
     * @param blackBrainFile File for Black Team Automaton
     * @param mapFile File for world map
     */
    FastPanel(final String redBrainFile, final String blackBrainFile, final String mapFile) {
        super();

        sim = new SwingWorker<Void,Void>() {
            public Void doInBackground() {
                Simulation s = new Simulation(mapFile, redBrainFile, blackBrainFile) {
                    @Override
                    protected void progressReport() {
                        setProgress(this.currentTick * 100 / NUM_TICKS);
                    };
                };
                SimulationResults res = s.run();
                setProgress(100);
                redFoodLabel.setText(Integer.toString(res.redCollected));
                blackFoodLabel.setText(Integer.toString(res.blackCollected));
                redAliveLabel.setText(Integer.toString(res.redAnts));
                blackAntsAlive.setText(Integer.toString(res.blackAnts));
                winnerLabel.setText(res.result.toString());
                return null;
            }
        };
        sim.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("progress" == evt.getPropertyName()) {
                    int progress = (Integer) evt.getNewValue();
                    progressBar.setValue(progress);
                }
            }
        });
        sim.execute();
    }

    /**
     * Constructs the fast simulation for panel from existing simulation
     * @param state Existing simulation
     */
    FastPanel(final Simulation state) {
        super();

        sim = new SwingWorker<Void,Void>() {
            public Void doInBackground() {
                Simulation s = new Simulation(state) {
                    @Override
                    protected void progressReport() {
                        setProgress(this.currentTick * 100 / NUM_TICKS);
                    };
                };
                SimulationResults res = s.run();
                setProgress(100);
                redFoodLabel.setText(Integer.toString(res.redCollected));
                blackFoodLabel.setText(Integer.toString(res.blackCollected));
                redAliveLabel.setText(Integer.toString(res.redAnts));
                blackAntsAlive.setText(Integer.toString(res.blackAnts));
                winnerLabel.setText(res.result.toString());
                return null;
            }
        };
        sim.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("progress" == evt.getPropertyName()) {
                    int progress = (Integer) evt.getNewValue();
                    progressBar.setValue(progress);
                }
            }
        });
        sim.execute();
    }

}
