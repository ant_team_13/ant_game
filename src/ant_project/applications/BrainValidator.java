package ant_project.applications;

import ant_project.fsa.Automaton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Class implementing application for validating Ant Brains
 */
public class BrainValidator {
    private JButton browseForBrainButton;
    private JTextField brainPathField;
    private JButton validateBrainButton;
    private JPanel mainPanel;
    private JFrame frame;

    /**
     * Creates the Brain Validator
     */
    public BrainValidator() {
        browseForBrainButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("."));
                int result = fileChooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    brainPathField.setText(selectedFile.getAbsolutePath());
                }
            }
        });
        validateBrainButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                String content = "";
                try {
                    content = new String(Files.readAllBytes(Paths.get(brainPathField.getText())));
                } catch (IOException e) {
                    showMessageDialog(null, "Error occurred: File was not found or could not be accessed.");
                    return;
                }
                try {
                    new Automaton(content);
                } catch (Exception e) {
                    showMessageDialog(null, "Error during script validation: " + e.getMessage());
                    return;
                }
                showMessageDialog(null, "Success. Brain script is valid.");
            }
        });

        frame = new JFrame("Brain Validator");

        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Dimension dimMinimum = frame.getMinimumSize();
        Dimension dimMaximum = frame.getMaximumSize();
        dimMinimum.setSize(700, 65);
        dimMaximum.setSize(2000, 65);
        frame.setMinimumSize(dimMinimum);
        frame.setMaximumSize(dimMaximum);
        frame.setLocationRelativeTo( null );
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Returns Main JFrame
     * @return Main JFrame
     */
    public JFrame getFrame() {
        return frame;
    }
}
