package ant_project.applications;

import ant_project.Generator;
import ant_project.Map;
import ant_project.renderer.MapCanvas;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.Random;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Class implementing the UI for map generation
 */
public class MapGeneratorPropPanel {
    private JButton saveAsButton;
    private JButton generateButton;
    private JTextField sideLenghtField;
    private JTextField foodBlobNField;
    private JTextField foodBlobMinLField;
    private JButton validTournamentParametersButton;
    private JButton randomizeSeedButton;
    private JTextField minFoodField;
    private JTextField maxFoodField;
    private JTextField foodBlobMaxLField;
    private JTextField seedField;
    public JPanel mapGeneratorPropPanel;
    private JTextField widthField;
    private JTextField heightField;
    private JProgressBar progressBar;
    private JTextField rocksField;

    private Random rand;
    private Random rand2;
    private MapCanvas canvas;
    private Map map;

    /**
     * Creates the map generator panel from map canvas
     * @param canvas
     */
    public MapGeneratorPropPanel(final MapCanvas canvas) {
        rand = new Random();
        rand2 = new Random();
        map = null;
        this.canvas = canvas;
        randomizeSeedButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                seedField.setText(Integer.toString(rand.nextInt()));
            }
        });
        validTournamentParametersButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                sideLenghtField.setText("7");
                foodBlobNField.setText("11");
                foodBlobMaxLField.setText("5");
                foodBlobMinLField.setText("5");
                minFoodField.setText("5");
                maxFoodField.setText("5");
                widthField.setText("150");
                heightField.setText("150");
                rocksField.setText("14");
            }
        });
        generateButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                try {
                    int tmp = Integer.parseInt(sideLenghtField.getText());
                    if (tmp < 1) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Anthill side length must be integer of value 1 or higher.");
                    return;
                }
                try {
                    int tmp = Integer.parseInt(foodBlobNField.getText());
                    if (tmp < 0) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Food blob number must be integer of value 0 or more.");
                    return;
                }
                try {
                    int tmp = Integer.parseInt(foodBlobMinLField.getText());
                    if (tmp < 1) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Minimum food blob side length must be integer of value 1 or more.");
                    return;
                }
                try {
                    int tmp = Integer.parseInt(foodBlobMinLField.getText());
                    int tmp2 = Integer.parseInt(foodBlobMaxLField.getText());
                    if (tmp2 < tmp) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Maximum food blob side length must be integer of at least same value as minimum side length.");
                    return;
                }
                try {
                    int tmp = Integer.parseInt(minFoodField.getText());
                    if (tmp < 1) { throw new IllegalArgumentException(); }
                    if (tmp > 9) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Minimum food in each cell must be integer of value 1 up to 9.");
                    return;
                }
                try {
                    int tmp = Integer.parseInt(minFoodField.getText());
                    int tmp2 = Integer.parseInt(maxFoodField.getText());
                    if (tmp2 < tmp) { throw new IllegalArgumentException(); }
                    if (tmp2 > 9) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Maximum food in each cell must be integer of at least same value as minimum amount of food and be less than 10.");
                    return;
                }
                try {
                    int tmp = Integer.parseInt(rocksField.getText());
                    if (tmp < 0) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Number of rocks must be integer of value 0 or more.");
                    return;
                }
                try {
                    int tmp = Integer.parseInt(widthField.getText());
                    if (tmp < 10) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Map Width must be integer of value 10 or more.");
                    return;
                }
                try {
                    int tmp = Integer.parseInt(heightField.getText());
                    if (tmp < 10) { throw new IllegalArgumentException(); }
                } catch (Exception e) {
                    showMessageDialog(null, "Map Height must be integer of value 10 or more.");
                    return;
                }
                try {
                    long tmp = Long.parseLong(seedField.getText());
                } catch (Exception e) {
                    showMessageDialog(null, "Randomization Seed must be integer.");
                    return;
                }

                try {
                    rand2.setSeed(Long.parseLong(seedField.getText()));
                    map = Generator.generate(
                            rand2,
                            Integer.parseInt(sideLenghtField.getText()),
                            Integer.parseInt(foodBlobNField.getText()),
                            Integer.parseInt(foodBlobMinLField.getText()),
                            Integer.parseInt(foodBlobMaxLField.getText()),
                            Integer.parseInt(minFoodField.getText()),
                            Integer.parseInt(maxFoodField.getText()),
                            Integer.parseInt(rocksField.getText()),
                            Integer.parseInt(widthField.getText()),
                            Integer.parseInt(heightField.getText()));
                    canvas.setMap(map);
                } catch (Exception e) {
                    showMessageDialog(null, "Error during map generation: " + e.getMessage());
                }
            }
        });
        saveAsButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (map != null) {
                    JFileChooser fileChooser = new JFileChooser();
                    fileChooser.setCurrentDirectory(new File("."));
                    int result = fileChooser.showSaveDialog(null);
                    if (result == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = fileChooser.getSelectedFile();
                        boolean exists = (new File(selectedFile.getAbsolutePath())).exists();
                        if (exists) {
                            int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to override existing file?", "Confirm",
                                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                            if (response == JOptionPane.NO_OPTION) {
                                return;
                            } else if (response == JOptionPane.YES_OPTION) {
                                System.out.println("Yes button clicked");
                            } else if (response == JOptionPane.CLOSED_OPTION) {
                                return;
                            }
                        }
                        try {
                            Writer out = new BufferedWriter(new FileWriter(selectedFile.getAbsolutePath()));
                            out.write(map.toString());
                            out.close();
                        } catch (FileNotFoundException e) {
                            showMessageDialog(null, "Error accessing the file.");
                        } catch (IOException e) {
                            showMessageDialog(null, "Error accessing the file.");
                        }
                    }
                }
            }
        });
    }
}
