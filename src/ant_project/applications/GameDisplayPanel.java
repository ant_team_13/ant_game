package ant_project.applications;

import ant_project.Simulation;
import ant_project.renderer.GameCanvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Class implementing UI for Graphical Game Simulation
 */
public class GameDisplayPanel implements ActionListener{
    private JButton slowerButton;
    private JButton fasterButton;
    private JButton finishSimulationButton;
    private JButton restartButton;
    private JButton newGameButton;
    private JLabel statusDisplay;
    public JPanel gameDisplayPanel;
    private JFrame parentFrame;
    private GameCanvas canvas;
    private Timer statusUpdate;

    /**
     * Constructs the Game display panel from game canvas and parent frame
     * @param canvas Game Canvas
     * @param parentFrame Parent Frame
     */
    public GameDisplayPanel(final GameCanvas canvas, final JFrame parentFrame) {
        this.canvas = canvas;
        this.parentFrame = parentFrame;
        slowerButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                canvas.slower();
            }
        });
        fasterButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                canvas.faster();
            }
        });
        statusUpdate = new Timer(100, this);
        statusUpdate.start();
        finishSimulationButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                SwingUtilities.getWindowAncestor(gameDisplayPanel).dispose();
                FastPanel fp = new FastPanel(canvas.getSimulation());
                final JFrame frameFast = new JFrame("Fast Game Simulation");
                frameFast.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frameFast.setVisible(false);
                        parentFrame.setVisible(true);
                    }});
                frameFast.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frameFast.setContentPane(fp.fastPanel);
                frameFast.pack();
                Dimension dimMinimumF = frameFast.getMinimumSize();
                Dimension dimMaximumF = frameFast.getMaximumSize();
                dimMinimumF.setSize(700, 205);
                dimMaximumF.setSize(2000, 205);
                frameFast.setMinimumSize(dimMinimumF);
                frameFast.setMaximumSize(dimMaximumF);
                frameFast.setLocationRelativeTo( null );
                frameFast.setResizable(false);
                frameFast.setVisible(true);
            }
        });
        restartButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(gameDisplayPanel, "Are you sure you want to restart the simulation? All progress will be lost.", "Warning",dialogButton);
                if(dialogResult == 0) {
                    canvas.restart();
                }
            }
        });
        newGameButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(gameDisplayPanel, "Are you sure you want to terminate current simulation? All progress will be lost.", "Warning",dialogButton);
                if(dialogResult == 0) {
                    SwingUtilities.getWindowAncestor(gameDisplayPanel).dispose();
                    parentFrame.setVisible(true);
                }
            }
        });
    }

    /**
     * Perform I/O handling
     * @param e Event to be handled
     */
    public void actionPerformed(ActionEvent e) {
        if(canvas != null) { statusDisplay.setText(canvas.toString()); }
        if(canvas != null && canvas.getSimulation().getCurrentTick() >= Simulation.NUM_TICKS) {
            SwingUtilities.getWindowAncestor(gameDisplayPanel).dispose();
            FastPanel fp = new FastPanel(canvas.getSimulation());
            canvas = null;
            final JFrame frameFast = new JFrame("Fast Game Simulation");
            frameFast.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    frameFast.setVisible(false);
                    parentFrame.setVisible(true);
                }});
            frameFast.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            frameFast.setContentPane(fp.fastPanel);
            frameFast.pack();
            Dimension dimMinimumF = frameFast.getMinimumSize();
            Dimension dimMaximumF = frameFast.getMaximumSize();
            dimMinimumF.setSize(700, 205);
            dimMaximumF.setSize(2000, 205);
            frameFast.setMinimumSize(dimMinimumF);
            frameFast.setMaximumSize(dimMaximumF);
            frameFast.setLocationRelativeTo( null );
            frameFast.setResizable(false);
            frameFast.setVisible(true);
        }
    }
}
