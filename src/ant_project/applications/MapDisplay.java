package ant_project.applications;

import ant_project.Map;
import ant_project.MapChecker;
import ant_project.fsa.Automaton;
import ant_project.renderer.MapCanvas;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl.LwjglCanvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static javax.swing.JOptionPane.showMessageDialog;


/**
 * Created by Daniel on 06/02/2015.
 */

/**
 * Class implementing application for displaying maps
 */
public class MapDisplay{
    private JFrame frame;
    private JFrame frameDisplay;

    private LwjglCanvas canvas;

    /**
     * Creates the Map Display
     */
    public MapDisplay()
    {
        super();
        final MapChooserPanel mp = new MapChooserPanel();
        frame = new JFrame("Map Display");


        mp.displayMapButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String mapFile = mp.getMapPath();

                {
                    String content = "";
                    try {
                        content = new String(Files.readAllBytes(Paths.get(mapFile)));
                    } catch (IOException ex) {
                        showMessageDialog(null, "Error occurred: File was not found or could not be accessed.");
                        return;
                    }
                    Map m;
                    try {
                        m = new Map(content);
                    } catch (Exception ex) {
                        showMessageDialog(null, "Error during map validation: " + ex.getMessage());
                        return;
                    }
                }
                frame.setVisible(false);
                LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
                config.title = "Map Display";
                config.width = 1024;
                config.height = 768;
                config.backgroundFPS = 144;
                config.foregroundFPS = 144;
                config.vSyncEnabled = false;
                final MapCanvas mc = new MapCanvas(mapFile);
                canvas = new LwjglCanvas(mc, config);

                frameDisplay = new JFrame("Map Display");
                frameDisplay.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        frame.setVisible(true);
                    }});
                frameDisplay.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                Container container = frameDisplay.getContentPane();
                container.add(canvas.getCanvas(), BorderLayout.CENTER);
                frameDisplay.pack();
                Dimension dimMinimumF = frameDisplay.getMinimumSize();
                Dimension dimMaximumF = frameDisplay.getMaximumSize();
                dimMinimumF.setSize(1024, 768);
                dimMaximumF.setSize(1024, 768);
                frameDisplay.setMinimumSize(dimMinimumF);
                frameDisplay.setMaximumSize(dimMaximumF);
                frameDisplay.setLocationRelativeTo( null );
                frameDisplay.setVisible(true);
            }
        });

        frame.setContentPane(mp.mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Dimension dimMinimum = frame.getMinimumSize();
        Dimension dimMaximum = frame.getMaximumSize();
        dimMinimum.setSize(700, 75);
        dimMaximum.setSize(2000, 75);
        frame.setMinimumSize(dimMinimum);
        frame.setMaximumSize(dimMaximum);
        frame.setLocationRelativeTo( null );
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Returns Main JFrame
     * @return Main JFrame
     */
    public JFrame getFrame() {
        return frame;
    }
}