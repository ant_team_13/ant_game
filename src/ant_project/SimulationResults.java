package ant_project;

/**
 * Created by User on 17/02/2015.
 */
public class SimulationResults {
    public enum Result{
        RED_WINS, BLACK_WINS, DRAW;
        @Override
        public String toString() {
            switch(this) {
                case RED_WINS:
                    return "Red wins!";
                case BLACK_WINS:
                    return "Black wins!";
                case DRAW:
                    return "Draw!";
            }
            return "";
        }
    }
    public Result result;
    public int redCollected; //Amount of food collected by red team
    public int blackCollected; //Amount of food collected by black team
    public int redAnts; //Amount of ants remaining in red team
    public int blackAnts; //Amount of ants remaining in black team
}
