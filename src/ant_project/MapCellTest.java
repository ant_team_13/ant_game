package ant_project;

import ant_project.Ant;
import ant_project.MapCell;
import ant_project.Vector2i;
import junit.framework.TestCase;

public class MapCellTest extends TestCase {
    public void testToString() throws Exception {

        MapCell testCell = new MapCell("#", new Vector2i(0,0));
        assertEquals(testCell.toString(), "#");

        testCell = new MapCell("+", new Vector2i(0,0));
        assertEquals(testCell.toString(), "+");

        testCell = new MapCell("-", new Vector2i(0,0));
        assertEquals(testCell.toString(), "-");

        testCell = new MapCell(".", new Vector2i(0,0));
        assertEquals(testCell.toString(), ".");

        testCell = new MapCell("1", new Vector2i(0,0));
        assertEquals(testCell.toString(), "1");

        testCell = new MapCell("2", new Vector2i(0,0));
        assertEquals(testCell.toString(), "2");

        testCell = new MapCell("3", new Vector2i(0,0));
        assertEquals(testCell.toString(), "3");

        testCell = new MapCell("4", new Vector2i(0,0));
        assertEquals(testCell.toString(), "4");

        testCell = new MapCell("5", new Vector2i(0,0));
        assertEquals(testCell.toString(), "5");

        testCell = new MapCell("6", new Vector2i(0,0));
        assertEquals(testCell.toString(), "6");

        testCell = new MapCell("7", new Vector2i(0,0));
        assertEquals(testCell.toString(), "7");

        testCell = new MapCell("8", new Vector2i(0,0));
        assertEquals(testCell.toString(), "8");

        testCell = new MapCell("9", new Vector2i(0,0));
        assertEquals(testCell.toString(), "9");

        testCell = new MapCell("10", new Vector2i(0,0));
        {boolean exc = true; try { testCell.toString(); } catch (Exception e) { exc = true; } assert(exc);}
    }

    public void testIsRocky() throws Exception {
        MapCell testCell = new MapCell("#", new Vector2i(0,0));
        assertTrue(testCell.isRocky());

        testCell = new MapCell("+", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("-", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell(".", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("1", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("2", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("3", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("4", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("5", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("6", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("7", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("8", new Vector2i(0,0));
        assertFalse(testCell.isRocky());

        testCell = new MapCell("9", new Vector2i(0,0));
        assertFalse(testCell.isRocky());
    }

    public void testIsAnthill() throws Exception {

        MapCell testCell = new MapCell("+", new Vector2i(0,0));
        assertTrue(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("-", new Vector2i(0,0));
        assertTrue(testCell.isAnthill(Ant.TeamColor.BLACK));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));

        testCell = new MapCell(".", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("1", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("2", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("3", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("4", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("5", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("6", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("7", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("8", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));

        testCell = new MapCell("9", new Vector2i(0,0));
        assertFalse(testCell.isAnthill(Ant.TeamColor.RED));
        assertFalse(testCell.isAnthill(Ant.TeamColor.BLACK));
    }


    public void testGetSetClearContainsAnt() throws Exception {
        MapCell testCell = new MapCell(".", new Vector2i(0,0));
        Ant a = new Ant(Ant.TeamColor.BLACK);
        assert(!testCell.containsAnt());
        assertEquals(testCell.getAnt(), null);
        testCell.setAnt(a);
        assert(testCell.containsAnt());
        assertEquals(testCell.getAnt(), a);
        testCell.clearAnt();
        assert(!testCell.containsAnt());
        assertEquals(testCell.getAnt(), null);

    }

    public void testSetMarker() throws Exception {
        MapCell testCell = new MapCell("#", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(),0);

        testCell.setFoodAmount(0);
        assertEquals(testCell.getFoodAmount(),0);

        testCell.setFoodAmount(1);
        assertEquals(testCell.getFoodAmount(),1);

        testCell.setFoodAmount(2);
        assertEquals(testCell.getFoodAmount(),2);

        testCell.setFoodAmount(3);
        assertEquals(testCell.getFoodAmount(),3);

        testCell.setFoodAmount(4);
        assertEquals(testCell.getFoodAmount(),4);

        testCell.setFoodAmount(5);
        assertEquals(testCell.getFoodAmount(),5);

        testCell.setFoodAmount(6);
        assertEquals(testCell.getFoodAmount(),6);

        testCell.setFoodAmount(7);
        assertEquals(testCell.getFoodAmount(),7);

        testCell.setFoodAmount(8);
        assertEquals(testCell.getFoodAmount(),8);

        testCell.setFoodAmount(9);
        assertEquals(testCell.getFoodAmount(),9);

        testCell = new MapCell("10", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(),10);
    }

    public void testClearSetMarker() throws Exception {
        MapCell testCell = new MapCell(".", new Vector2i(0,0));
        assert(!testCell.checkMarker(Ant.TeamColor.RED));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,0));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,1));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,2));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,3));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,4));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,5));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,0));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,1));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,2));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,3));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,4));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,5));

        testCell.setMarker(Ant.TeamColor.RED, 0);
        assert(testCell.checkMarker(Ant.TeamColor.RED));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,0));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,1));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,2));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,3));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,4));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,5));
        assert(testCell.checkMarker(Ant.TeamColor.RED,0));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,1));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,2));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,3));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,4));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,5));

        testCell.setMarker(Ant.TeamColor.BLACK, 0);
        assert(testCell.checkMarker(Ant.TeamColor.RED));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK,0));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,1));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,2));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,3));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,4));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,5));
        assert(testCell.checkMarker(Ant.TeamColor.RED,0));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,1));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,2));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,3));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,4));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,5));

        testCell.setMarker(Ant.TeamColor.RED, 5);
        assert(testCell.checkMarker(Ant.TeamColor.RED));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK,0));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,1));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,2));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,3));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,4));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,5));
        assert(testCell.checkMarker(Ant.TeamColor.RED,0));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,1));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,2));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,3));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,4));
        assert(testCell.checkMarker(Ant.TeamColor.RED,5));

        testCell.setMarker(Ant.TeamColor.BLACK, 5);
        assert(testCell.checkMarker(Ant.TeamColor.RED));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK,0));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,1));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,2));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,3));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,4));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK,5));
        assert(testCell.checkMarker(Ant.TeamColor.RED,0));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,1));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,2));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,3));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,4));
        assert(testCell.checkMarker(Ant.TeamColor.RED,5));

        testCell.clearMarker(Ant.TeamColor.RED, 0);
        assert(testCell.checkMarker(Ant.TeamColor.RED));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK,0));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,1));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,2));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,3));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,4));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK,5));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,0));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,1));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,2));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,3));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,4));
        assert(testCell.checkMarker(Ant.TeamColor.RED,5));

        testCell.clearMarker(Ant.TeamColor.RED, 5);
        assert(!testCell.checkMarker(Ant.TeamColor.RED));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK,0));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,1));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,2));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,3));
        assert(!testCell.checkMarker(Ant.TeamColor.BLACK,4));
        assert(testCell.checkMarker(Ant.TeamColor.BLACK,5));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,0));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,1));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,2));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,3));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,4));
        assert(!testCell.checkMarker(Ant.TeamColor.RED,5));
    }

    public void testGetFoodAmount() throws Exception {
        MapCell testCell = new MapCell("#", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(),0);

        testCell = new MapCell("+", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(),0);

        testCell = new MapCell("-", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 0);

        testCell = new MapCell(".", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 0);

        testCell = new MapCell("1", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 1);

        testCell = new MapCell("2", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 2);

        testCell = new MapCell("3", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 3);

        testCell = new MapCell("4", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 4);

        testCell = new MapCell("5", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 5);

        testCell = new MapCell("6", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 6);

        testCell = new MapCell("7", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 7);

        testCell = new MapCell("8", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 8);

        testCell = new MapCell("9", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 9);

        testCell = new MapCell("10", new Vector2i(0,0));
        assertEquals(testCell.getFoodAmount(), 10);
    }

}