/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project;

import ant_project.fsa.Automaton;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Class for managing the Ants and updating their position throughout the game.
 */
public class AntManager {

    private List<Ant> ants;
    private Automaton redAutomaton;
    private Automaton blackAutomaton;
    private Map map;

    /**
     * Constructs the ant from already existing automatons and map.
     * @param redAutomaton Automaton to be used for red ant team.
     * @param blackAutomaton Automaton to be used for black ant team.
     * @param map Map to be used for the game.
     */
    public AntManager(Automaton redAutomaton, Automaton blackAutomaton, Map map) {

        ants = new LinkedList();
        this.redAutomaton = redAutomaton;
        this.blackAutomaton = blackAutomaton;
        this.map = map;

        for (int y = 0; y < map.getHeight(); ++y) {
            for (int x = 0; x < map.getWidth(); ++x) {
                MapCell currentCell = map.getCell(x, y);

                if (currentCell.isAnthill(Ant.TeamColor.RED)) {
                    Ant newAnt = new Ant(Ant.TeamColor.RED);
                    newAnt.setPosition(currentCell);
                    currentCell.setAnt(newAnt);
                    ants.add(newAnt);

                } else if (currentCell.isAnthill(Ant.TeamColor.BLACK)) {
                    Ant newAnt = new Ant(Ant.TeamColor.BLACK);
                    newAnt.setPosition(currentCell);
                    currentCell.setAnt(newAnt);
                    ants.add(newAnt);
                }
            }
        }
    }

    /**
     * Executes a single cycle of the game
     */
    public void tick() {
        for (Ant a : ants) {
            if (a.getPosition() != null && a.getColor() == Ant.TeamColor.BLACK) { blackAutomaton.execute(a, map); }
            else if (a.getPosition() != null && a.getColor() == Ant.TeamColor.RED) { redAutomaton.execute(a, map); }
        }
    }

    /**
     * Returns the map used for the game cycle execution.
     * @return World Map
     */
    public Map getMap() {
        return map;
    }
    public List<Ant> getAnts() { return ants; }
}
