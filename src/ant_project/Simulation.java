/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ant_project;

import ant_project.fsa.Automaton;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidParameterException;

/**
 * Class executing the game simulation
 */
public class Simulation {
    public static final int NUM_TICKS = 300000;
    private AntManager manager;
    protected int currentTick;

    /**
     * Constructs the game simulation from existing map and automatons
     * @param map Map on which the simulation should be executed
     * @param redAutomaton Automaton for red ant team
     * @param blackAutomaton Automaton for black ant team
     */
    public Simulation(Map map, Automaton redAutomaton, Automaton blackAutomaton) {
        manager = new AntManager(redAutomaton, blackAutomaton, map);
    }

    public Simulation(Simulation simulation) {
        this.manager = simulation.manager;
        this.currentTick = simulation.currentTick;
    }

    /**
     * Constructs the game simulation from map and automatons which are loaded from files
     * @param mapFile Map file
     * @param redAutomatonFile Red ant team automaton file
     * @param blackAutomatonFile Black ant team automaton file
     */
    public Simulation(String mapFile, String redAutomatonFile, String blackAutomatonFile) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(mapFile)));
        } catch (IOException e) {
            throw new InvalidParameterException("Could not read the file " + Paths.get(mapFile));
        }
        Map map = new Map(content);

        try {
            content = new String(Files.readAllBytes(Paths.get(redAutomatonFile)));
        } catch (IOException e) {
            throw new InvalidParameterException("Could not read the file " + Paths.get(redAutomatonFile));
        }
        Automaton redA = new Automaton(content);

        try {
            content = new String(Files.readAllBytes(Paths.get(blackAutomatonFile)));
        } catch (IOException e) {
            throw new InvalidParameterException("Could not read the file " + Paths.get(blackAutomatonFile));
        }
        Automaton blackA = new Automaton(content);
        manager = new AntManager(redA, blackA, map);
    }

    /**
     * Gets the map used for the game simulation
     * @return Map used for the game simulation
     */
    public Map getMap() { return manager.getMap(); }

    /**
     * Optional method that can be implemented to give feedback about progress of the simulation (called on each game
     * tick)
     */
    protected void progressReport() {}

    /**
     * Runs the full game simulation and returns the results
     * @return Results of the simulation
     */
    public SimulationResults run() {
        return runN(NUM_TICKS);
    }

    /**
     * Builds the results of the simulation
     * @return Results of the simulation
     */
    public SimulationResults buildResult() {
        SimulationResults sr = new SimulationResults();
        sr.blackAnts = 0;
        sr.redAnts = 0;
        sr.blackCollected = 0;
        sr.redCollected = 0;

        for (int y = 0; y < manager.getMap().getHeight(); ++y) {
            for (int x = 0; x < manager.getMap().getWidth(); ++x) {
                MapCell cell = manager.getMap().getCell(x, y);
                if (cell.isAnthill(Ant.TeamColor.RED)) {
                    sr.redCollected += cell.getFoodAmount();
                } else if (cell.isAnthill(Ant.TeamColor.BLACK)) {
                    sr.blackCollected += cell.getFoodAmount();
                }
                if (cell.containsAnt()) {
                    if (cell.getAnt().getColor() == Ant.TeamColor.RED) {
                        ++sr.redAnts;
                    } else if (cell.getAnt().getColor() == Ant.TeamColor.BLACK) {
                        ++sr.blackAnts;
                    }
                }
            }
        }
        if (sr.blackCollected == sr.redCollected) {
            sr.result = SimulationResults.Result.DRAW;
        } else if (sr.blackCollected < sr.redCollected) {
            sr.result = SimulationResults.Result.RED_WINS;
        } else {
            sr.result = SimulationResults.Result.BLACK_WINS;
        }
        return sr;
    }

    /**
     * Progresses the game simulation by given number of ticks
     * @param ticks Number of ticks to progress
     * @return Returns results of the simulation if the simulation finished, null otherwise
     */
    public SimulationResults runN(int ticks) {
        for (int tick = 0;currentTick < NUM_TICKS && tick < ticks; ++currentTick, ++tick) {
            manager.tick();
            progressReport();
        }
        if (currentTick < NUM_TICKS) { return null; }
        return buildResult();
    }

    /**
     * Returns the progress of the simulation in terms of ticks elapsed
     * @return Ticks elapsed since start of the simulation
     */
    public int getCurrentTick() {
        return currentTick;
    }
}
