package ant_project;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import ant_project.Ant;
import ant_project.MapCell;
import ant_project.Vector2i;
import org.junit.Test;

/**
 * Created by Curt on 21/03/2015.
 */
public class AntTest {


    @Test
    public void testRotateLeft() throws Exception {

        Ant.Direction testDir = new Ant.Direction(0);
        testDir.rotateLeft();
        assertEquals(testDir.getDir(), new Ant.Direction(5).getDir());

        testDir = new Ant.Direction(1);
        testDir.rotateLeft();
        assertEquals(testDir.getDir(), new Ant.Direction(0).getDir());

        testDir = new Ant.Direction(2);
        testDir.rotateLeft();
        assertEquals(testDir.getDir(), new Ant.Direction(1).getDir());

        testDir = new Ant.Direction(3);
        testDir.rotateLeft();
        assertEquals(testDir.getDir(), new Ant.Direction(2).getDir());

        testDir = new Ant.Direction(4);
        testDir.rotateLeft();
        assertEquals(testDir.getDir(), new Ant.Direction(3).getDir());

        testDir = new Ant.Direction(5);
        testDir.rotateLeft();
        assertEquals(testDir.getDir(), new Ant.Direction(4).getDir());

    }

    @Test
    public void testRotateRight() throws Exception {

        Ant.Direction testDir = new Ant.Direction(0);
        testDir.rotateRight();
        assertEquals(testDir.getDir(), new Ant.Direction(1).getDir());

        testDir = new Ant.Direction(1);
        testDir.rotateRight();
        assertEquals(testDir.getDir(), new Ant.Direction(2).getDir());

        testDir = new Ant.Direction(2);
        testDir.rotateRight();
        assertEquals(testDir.getDir(), new Ant.Direction(3).getDir());

        testDir = new Ant.Direction(3);
        testDir.rotateRight();
        assertEquals(testDir.getDir(), new Ant.Direction(4).getDir());

        testDir = new Ant.Direction(4);
        testDir.rotateRight();
        assertEquals(testDir.getDir(), new Ant.Direction(5).getDir());

        testDir = new Ant.Direction(5);
        testDir.rotateRight();
        assertEquals(testDir.getDir(), new Ant.Direction(0).getDir());
    }

    @Test
    public void testGetDir() throws Exception {

        Ant.Direction testDir = new Ant.Direction(0);
        assertEquals(testDir.getDir(), 0);

        testDir = new Ant.Direction(1);
        assertEquals(testDir.getDir(), 1);

        testDir = new Ant.Direction(2);
        assertEquals(testDir.getDir(), 2);

        testDir = new Ant.Direction(3);
        assertEquals(testDir.getDir(), 3);

        testDir = new Ant.Direction(4);
        assertEquals(testDir.getDir(), 4);

        testDir = new Ant.Direction(5);
        assertEquals(testDir.getDir(), 5);

    }

    @Test
    public void testFromInt() throws Exception {

        Ant.Direction testDir = Ant.Direction.fromInt(0);
        assertEquals(testDir.getDir(), 0);

        testDir = Ant.Direction.fromInt(1);
        assertEquals(testDir.getDir(), 1);

        testDir = Ant.Direction.fromInt(2);
        assertEquals(testDir.getDir(), 2);

        testDir = Ant.Direction.fromInt(3);
        assertEquals(testDir.getDir(), 3);

        testDir = Ant.Direction.fromInt(4);
        assertEquals(testDir.getDir(), 4);

        testDir = Ant.Direction.fromInt(5);
        assertEquals(testDir.getDir(), 5);

    }

    @Test
    public void testGetLeft() throws Exception {

        Ant.Direction testDir = new Ant.Direction(0);
        assertEquals(testDir.getLeft().getDir(), new Ant.Direction(5).getDir());

        testDir = new Ant.Direction(1);
        assertEquals(testDir.getLeft().getDir(), new Ant.Direction(0).getDir());

        testDir = new Ant.Direction(2);
        assertEquals(testDir.getLeft().getDir(), new Ant.Direction(1).getDir());

        testDir = new Ant.Direction(3);
        assertEquals(testDir.getLeft().getDir(), new Ant.Direction(2).getDir());

        testDir = new Ant.Direction(4);
        assertEquals(testDir.getLeft().getDir(), new Ant.Direction(3).getDir());

        testDir = new Ant.Direction(5);
        assertEquals(testDir.getLeft().getDir(), new Ant.Direction(4).getDir());

    }

    @Test
    public void testGetRight() throws Exception {

        Ant.Direction testDir = new Ant.Direction(0);
        assertEquals(testDir.getRight().getDir(), new Ant.Direction(1).getDir());

        testDir = new Ant.Direction(1);
        assertEquals(testDir.getRight().getDir(), new Ant.Direction(2).getDir());

        testDir = new Ant.Direction(2);
        assertEquals(testDir.getRight().getDir(), new Ant.Direction(3).getDir());

        testDir = new Ant.Direction(3);
        assertEquals(testDir.getRight().getDir(), new Ant.Direction(4).getDir());

        testDir = new Ant.Direction(4);
        assertEquals(testDir.getRight().getDir(), new Ant.Direction(5).getDir());

        testDir = new Ant.Direction(5);
        assertEquals(testDir.getRight().getDir(), new Ant.Direction(0).getDir());

    }


    @Test
    public void testOpposite() throws Exception {

        Ant.TeamColor red = Ant.TeamColor.RED;
        Ant.TeamColor redOpposite = red.opposite();

        Ant.TeamColor black = Ant.TeamColor.BLACK;
        Ant.TeamColor blackOpposite = black.opposite();

        assertEquals(redOpposite, black);
        assertEquals(blackOpposite, red);
    }

    @Test
    public void testOpposite_static() throws Exception {

        Ant.TeamColor red = Ant.TeamColor.RED;
        Ant.TeamColor black = Ant.TeamColor.BLACK;

        assertEquals(Ant.TeamColor.opposite(red), black);
        assertEquals(Ant.TeamColor.opposite(black), red);
    }

    @Test
    public void testGetSetState() throws Exception {
        Ant a = new Ant(Ant.TeamColor.BLACK);
        assertEquals(a.getState(),0);
        a.setState((short)100);
        assertEquals(a.getState(),100);
        a.setState((short)1);
        assertEquals(a.getState(),1);
        a.setState((short) 9999);
        assertEquals(a.getState(), 9999);
        a.setState((short) 0);
        assertEquals(a.getState(), 0);
    }

    @Test
    public void testGetSetHasFood() throws Exception {
        Ant a = new Ant(Ant.TeamColor.BLACK);
        assertEquals(a.hasFood(),false);
        a.setHasFood(false);
        assertEquals(a.hasFood(),false);
        a.setHasFood(true);
        assertEquals(a.hasFood(),true);
        a.setHasFood(true);
        assertEquals(a.hasFood(),true);
        a.setHasFood(false);
        assertEquals(a.hasFood(), false);
    }

    @Test
    public void testGetSetResting() throws Exception {
        Ant a = new Ant(Ant.TeamColor.BLACK);
        assertEquals(a.getResting(),0);
        a.setResting(14);
        assertEquals(a.getResting(),14);
        a.setResting(13);
        assertEquals(a.getResting(), 13);
        a.setResting(13);
        assertEquals(a.getResting(), 13);
        a.setResting(0);
        assertEquals(a.getResting(),0);
    }

    @Test
    public void testKill() throws Exception {
        {
            Ant a = new Ant(Ant.TeamColor.BLACK);
            MapCell mc = new MapCell("+",new Vector2i(0,0));
            assertEquals(mc.getFoodAmount(), 0);
            assertEquals(a.hasFood(), false);
            a.setPosition(mc);
            mc.setAnt(a);
            a.kill();
            assertEquals(a.getPosition(), null);
            assertEquals(mc.getAnt(), null);
            assertEquals(mc.getFoodAmount(), 3);
        }
        {
            Ant a = new Ant(Ant.TeamColor.BLACK);
            MapCell mc = new MapCell("+",new Vector2i(0,0));
            a.setHasFood(true);
            assertEquals(mc.getFoodAmount(), 0);
            assertEquals(a.hasFood(), true);
            a.setPosition(mc);
            mc.setAnt(a);
            a.kill();
            assertEquals(a.getPosition(), null);
            assertEquals(mc.getAnt(), null);
            assertEquals(mc.getFoodAmount(), 4);
        }
    }

    @Test
    public void testGetSetPosition() throws Exception {
        Ant a = new Ant(Ant.TeamColor.BLACK);
        MapCell mc = new MapCell("+",new Vector2i(0,0));
        assertEquals(a.getPosition(), null);
        a.setPosition(mc);
        assertEquals(a.getPosition(), mc);
        a.setPosition(mc);
        assertEquals(a.getPosition(), mc);
        a.setPosition(null);
        assertEquals(a.getPosition(), null);

    }
}
