/**
 * Created by Daniel Saska on 22/02/2015
 */
package ant_project;

import java.math.BigInteger;

/**
 * Class providing proprietary random numbers
 */
public class Rng {

    private static int seed = 0;
    //private static BigInteger hiddenVal;
    //private static boolean initialized = false;

    private static int s = 0;
    private static boolean initialized = false;

    /**
     * Generates next random integer in sequence from given integer value
     * @param mod Given integer value
     * @return Next random integer in sequence
     */
    public static int randomInt(int mod) {
        /*
        if (!initialized) {
            hiddenVal = BigInteger.valueOf(seed);
            for (int i = 0; i < 4; ++i) { //get S4 to start with
                hiddenVal = hiddenVal.multiply(BigInteger.valueOf(22695477)).add(BigInteger.valueOf(1));
            }
            initialized = true;
        }
        BigInteger publicVal = hiddenVal.divide(BigInteger.valueOf(65536)).mod(BigInteger.valueOf(16384));
        BigInteger last = publicVal.mod(BigInteger.valueOf(mod));

        hiddenVal = hiddenVal.multiply(BigInteger.valueOf(22695477)).add(BigInteger.valueOf(1));

        return last.intValue();
        */

        if (!initialized) {
            for (int i = 0; i < 4; ++i) { //get S4 to start with
                s = s * 22695477 + 1;
            }
            initialized = true;
        }
        int ret = ((s / 65536) % 16384) % mod;
        if (ret < 0) { ret += mod - 1; } //Positive is always desired
        s = s * 22695477 + 1;
        return ret;
    }

    /**
     * Sets the initial seed of the random number generator
     * @param seed The value of the initial seed
     */
    public static void setSeed(int seed) {
        Rng.seed = seed;
        Rng.s = seed;
        initialized = false;
    }

    /**
     * Gets the value of the initial seed
     * @return The value of the initial seed
     */
    public static int getSeed() {
        return Rng.seed;
    }
}
