package ant_project;

import ant_project.fsa.Automaton;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Scanner;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by calum on 05/04/2015.
 */
public class Tournament {

    private ArrayList<String> maps;
    private ArrayList<String> brainNames;
    private ArrayList<Automaton> brains;
    private int[] scores;
    protected int gamesTotal;
    protected int gamesDone;
    protected int gameProgress;

    public Tournament(String mapsDir, String brainsDir) {
        maps = new ArrayList<String>();
        brains = new ArrayList<Automaton>();
        brainNames = new ArrayList<String>();

        File mapFiles[];
        File mapDir = new File(mapsDir);
        mapFiles = mapDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".world");
            }
        });
        if (mapFiles == null) { throw new InvalidParameterException("Map directory could not be accessed or found"); }
        File brainFiles[];
        File brainDir = new File(brainsDir);
        brainFiles = brainDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename)
            { return filename.endsWith(".ant"); }
        } );
        if (brainFiles == null) { throw new InvalidParameterException("Map directory could not be accessed or found"); }

        for (File f : mapFiles) {
            String content = "";
            try {
                content = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())));
            } catch (IOException e) {
                throw new InvalidParameterException("Error occurred: File was not found or could not be accessed ("+f.getName()+").");
            }
            Map m;
            try {
                m = new Map(content);
            } catch (Exception e) {
                throw new InvalidParameterException("Error during map validation ("+f.getName()+"): " + e.getMessage());
            }
            try {
                MapChecker.check(m);
            } catch (Exception e) {
                throw new InvalidParameterException("Map file format is valid but map does not satisfy tournament conditions ("+f.getName()+"): " + e.getMessage());
            }
            maps.add(content);
        }

        for (File f : brainFiles) {
            String content = "";
            try {
                content = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())));
            } catch (IOException ex) {
                throw new InvalidParameterException("Error occurred: File was not found or could not be accessed ("+f.getName()+").");
            }
            try {
                new Automaton(content);
            } catch (Exception ex) {
                throw new InvalidParameterException("Error during script validation ("+f.getName()+"): " + ex.getMessage());
            }
            brains.add(new Automaton(content));
            brainNames.add(f.getName());
        }
        if (maps.size() == 0) { throw new InvalidParameterException("No maps found."); }
        if (brains.size() < 2) { throw new InvalidParameterException("Not enough brain scripts found. Minimum is 2."); }
        gamesTotal = maps.size() * (brains.size() - 1) * brains.size();
        gamesDone = 0;
        scores = new int[brains.size()];
    }

    /**
     *
     */
    public void runTournament() {
        for (String map : maps) {
            for (int i = 0; i < brains.size() - 1; i++) {
                for (int j = i + 1; j < brains.size(); j++) {

                    Simulation redI = new Simulation(new Map(map), brains.get(i), brains.get(j)) {
                        @Override
                        protected void progressReport() {
                            gameProgress = (this.currentTick * 100 / NUM_TICKS);
                            gameUpdate();
                        }
                    };
                    if (redI.run().result == SimulationResults.Result.RED_WINS) {
                        scores[i] = scores[i] + 2;
                    } else if (redI.run().result == SimulationResults.Result.BLACK_WINS) {
                        scores[j] = scores[j] + 2;
                    } else if (redI.run().result == SimulationResults.Result.DRAW) {
                        scores[i]++;
                        scores[j]++;
                    }
                    gameProgress = 100;
                    ++gamesDone;
                    gameUpdate();
                    Simulation redJ = new Simulation(new Map(map), brains.get(j), brains.get(i)) {
                        @Override
                        protected void progressReport() {
                            gameProgress = (this.currentTick * 100 / NUM_TICKS);
                            gameUpdate();
                        }
                    };
                    if (redJ.run().result == SimulationResults.Result.RED_WINS) {
                        scores[j] = scores[j] + 2;
                    } else if (redJ.run().result == SimulationResults.Result.BLACK_WINS) {
                        scores[i] = scores[i] + 2;
                    } else if (redJ.run().result == SimulationResults.Result.DRAW) {
                        scores[j]++;
                        scores[i]++;
                    }
                    gameProgress = 100;
                    ++gamesDone;
                    gameUpdate();
                }
            }
        }
    }

    protected void gameUpdate() {

    }

    public String[] getWinners(){
        int first = -1, second = -1, third = -1;

        for(int i = 0; i < scores.length; ++i) {
            if (first == -1) {
                first = i;
            } else if(scores[i] > scores[first]) {
                third = second;
                second = first;
                first = i;
            } else if (second == -1) {
                second = i;
            } else if (scores[i] > scores[second]) {
                third = second;
                second = i;
            }  else if (third == -1) {
                third = i;
            } else if (scores[i] > scores[third]) {
                third  = i;
            }
        }

        String firstWinner = "";
        if (first != -1) {
            firstWinner = brainNames.get(first) + " ("+ Integer.toString(scores[first]) +" points)";
        }
        String secondWinner = "";
        if (second != -1) {
            secondWinner = brainNames.get(second) + " ("+ Integer.toString(scores[second]) +" points)";
        }
        String thirdWinner = "";
        if (third != -1) {
            thirdWinner = brainNames.get(third) + " ("+ Integer.toString(scores[third]) +" points)";
        }

        return new String[] { firstWinner, secondWinner, thirdWinner};
    }
}