
package ant_project.renderer;

import ant_project.Map;
import ant_project.renderer.CameraView;
import ant_project.renderer.MapRenderer;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglCanvas;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.swing.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
/**
 * Created by Daniel on 06/02/2015.
 */

/**
 * Class implementing UI element for drawing map on a screen
 */
public class MapCanvas implements ApplicationListener {

    private SpriteBatch batch;
    private BitmapFont font;
    private MapRenderer mr;
    private CameraView cv;
    private Viewport viewport;
    private Map m;
    private OrthographicCamera camera;
    private String mapFile;

    /**
     * Constructs the map canvas from map file
     * @param mapFile Map file
     */
    public MapCanvas(String mapFile)
    {
        super();
        this.mapFile = mapFile;
    }

    /**
     * Zero-initializes the map canvas
     */
    public MapCanvas()
    {
        super();
        this.mapFile = null;
    }

    /**
     * Creates the map canvas from previously provided map/file
     */
    @Override
    public void create() {

        batch = new SpriteBatch();
        font = new BitmapFont();
        font.setColor(Color.RED);

        camera = new OrthographicCamera();
        viewport = new ScreenViewport(camera);
        viewport.apply();
        camera.position.set(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2,0);

        if (mapFile != null) {
            String content = "";
            try {
                content = new String(Files.readAllBytes(Paths.get(mapFile)));
            } catch (IOException e) {
                e.printStackTrace();
            }
            m = new Map(content);
        } else {
            m = new Map();
        }

        cv = new CameraView((6 * m.getWidth() / 2) - Gdx.graphics.getWidth()/2,(4 * m.getWidth() / 2) - Gdx.graphics.getWidth()/2, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        mr = new MapRenderer(6,4,3,batch,m,cv);
    }

    /**
     * Disposes of unnecessary elements
     */
    @Override
    public void dispose() {
    }

    FPSLogger logger = new FPSLogger();

    /**
     * Renders a frame of the map canvas
     */
    @Override
    public void render() {

        logger.log();
        camera.update();
        Gdx.gl20.glClearColor(1, 1, 1, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        float delta = Gdx.graphics.getDeltaTime();
        cv.update(delta);
        mr.update(delta);
        batch.setProjectionMatrix(camera.combined);
        batch.enableBlending();
        batch.begin();
        mr.render();
        batch.end();
    }

    /**
     * Resizes the map canvas
     * @param width New canvas width
     * @param height New canvas height
     */
    @Override
    public void resize(int width, int height) {
        viewport.update(width,height);
        cv.setWB(width,height);
        mr = new MapRenderer(6,4,3,batch,m,cv);
        camera.position.set(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2,0);
    }

    /**
     * Pauses the map view
     */
    @Override
    public void pause() {
    }

    /**
     * Resumes the map view
     */
    @Override
    public void resume() {
    }

    /**
     * Sets the map to be drawn
     * @param map Map to be drawn
     */
    public void setMap(Map map) {
        m = map;
        mr.setMap(map);
        cv.setPosition((6 * m.getWidth() / 2) - Gdx.graphics.getWidth()/2,(4 * m.getWidth() / 2) - Gdx.graphics.getWidth()/2);
    }
}