package ant_project.renderer;

import ant_project.Ant;
import ant_project.Map;
import ant_project.MapCell;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Daniel on 06/02/2015.
 */

/**
 * Class handling the rendering of the map on the screen
 */
public class MapRenderer {
    private int xSpacing;
    private int ySpacing;
    private int evenOffset;
    private Map map;
    private CameraView view;

    private SpriteBatch batch;

    private TextureAtlas atlas;
    private TextureRegion redAntTile;
    private TextureRegion blackAntTile;
    private TextureRegion redAnthillTile;
    private TextureRegion blackAnthillTile;
    private TextureRegion emptyTile;
    private TextureRegion foodTile;
    private TextureRegion obstacleTile;


    private Sprite[] sprites;
    private int spritesHorizontal;
    private int spritesVertical;

    /**
     * Constructs the renderer
     * @param xSpacing Spacing of cells in X dimension
     * @param ySpacing Spacing of cells in Y dimension
     * @param evenOffset Offset of even rows
     * @param batch Sprite sheet of textures to be used
     * @param map Map to be rendered on the screen
     * @param view Camera view
     */
    public MapRenderer(int xSpacing, int ySpacing, int evenOffset, SpriteBatch batch, Map map, CameraView view) {
        this.xSpacing = xSpacing;
        this.ySpacing = ySpacing;
        this.evenOffset = evenOffset;
        this.batch = batch;
        this.map = map;
        this.view = view;

        spritesVertical = ((int)(view.getHeight() / ySpacing)) + 4; //Number of sprites in vertical direction
        spritesHorizontal = ((int)(view.getWidth() / xSpacing)) + 4; //Number of sprites in horizontal direction
        sprites = new Sprite[spritesHorizontal * spritesVertical];


        atlas = new TextureAtlas("data/textures/textures.pack");
        obstacleTile = atlas.findRegion("hex_obstacle");
        foodTile = atlas.findRegion("hex_food");
        emptyTile = atlas.findRegion("hex_empty");
        redAntTile = atlas.findRegion("hex_red_ant");
        redAnthillTile = atlas.findRegion("hex_red_hill");
        blackAntTile = atlas.findRegion("hex_black_ant");
        blackAnthillTile = atlas.findRegion("hex_black_hill");

        for (int i = 0; i < spritesVertical * spritesHorizontal; ++i) {
            sprites[i] = new Sprite(obstacleTile);
        }
    }

    /**
     * Updates the map on the screen
     * @param delta Time elapsed since last update
     */
    public void update(float delta) {
        //Logical position
        int bottomLogical = (int)view.getBottom() / ySpacing;
        int leftLogical = (int)view.getLeft() / xSpacing;
        //Graphical offset
        float xOffsetGraphical = (float)(leftLogical * xSpacing) - view.getLeft();
        float yOffsetGraphical = (float)(bottomLogical * ySpacing) - view.getBottom();

        for (int y = 0; y < spritesVertical; ++y) {
            for (int x = 0; x < spritesHorizontal; ++x) {
                int xLogical = x + leftLogical - 2;
                int yLogical = y + bottomLogical - 2;
                MapCell cell = map.getCellBL(xLogical, yLogical);
                if (cell == null || cell.isRocky()) {
                    sprites[x + y * spritesHorizontal].setRegion(obstacleTile);
                } else if (cell.containsAnt()) {
                    if (cell.getAnt().getColor() == Ant.TeamColor.BLACK) {
                        sprites[x + y * spritesHorizontal].setRegion(blackAntTile);
                    } else {
                        sprites[x + y * spritesHorizontal].setRegion(redAntTile);
                    }
                } else if (cell.getFoodAmount() > 0) {
                    sprites[x + y * spritesHorizontal].setRegion(foodTile);
                } else if (cell.isAnthill(Ant.TeamColor.BLACK)) {
                    sprites[x + y * spritesHorizontal].setRegion(blackAnthillTile);
                } else if (cell.isAnthill(Ant.TeamColor.RED)) {
                    sprites[x + y * spritesHorizontal].setRegion(redAnthillTile);
                }
                else {
                    sprites[x + y * spritesHorizontal].setRegion(emptyTile);
                }
                float offset = map.isEvenBL(yLogical) ? evenOffset : 0;
                sprites[x + y * spritesHorizontal].setX((x - 2) * xSpacing + xOffsetGraphical + offset);
                sprites[x + y * spritesHorizontal].setY((y - 2) * ySpacing + yOffsetGraphical);
            }
        }
    }

    /**
     * Renders the map content on screen
     */
    public void render() {
        for (Sprite sprite : sprites) {
            sprite.draw(batch);
        }
    }

    /**
     * Sets the map to be that which is specified in parameter
     * @param map Map to be used
     */
    public void setMap(Map map) {
        this.map = map;
    }
}