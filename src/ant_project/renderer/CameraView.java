package ant_project.renderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

/**
 * Created by Daniel on 06/02/2015.
 */

/**
 * Custom camera handling class
 */
public class CameraView {
    private float x;
    private float y;
    private int w;
    private int h;

    /**
     * Constructs the camera view
     * @param x Camera X position
     * @param y Camera Y position
     * @param width Width of the canvas
     * @param height Height of the canvas
     */
    public CameraView(float x, float y, int width, int height) {
        this.x = x;
        this.y = y;
        this.w = width;
        this.h = height;
    }

    /**
     * Sets the position of the camera
     * @param x Camera X position
     * @param y Camera Y position
     */
    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Moves the camera by vector specified
     * @param x Movement in X dimension
     * @param y Movement in Y dimension
     */
    public void move(float x, float y) {
        this.x += x;
        this.y += y;
    }

    /**
     * Updates the width and height of the canvas
     * @param width Width of the canvas
     * @param height Height of the canvas
     */
    public void setWB(int width, int height) {
        this.x -= ((float)width - w) / 2;
        this.y -= ((float)height - h) / 2;
        this.w = width;
        this.h = height;
    }

    /**
     * Updates the camera
     * @param delta Time elapsed since last update
     */
    public void update(float delta) {
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)){ x -= 200 * delta; }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)){ x += 200 * delta; }
        if(Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)){ y += 200 * delta; }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S)){ y -= 200 * delta; }
    }

    /**
     * Gets the top coordinate of the view window
     * @return Top coordinate
     */
    public float getTop() { return y + h; }

    /**
     * Gets the left coordinate of the view window
     * @return Left coordinate
     */
    public float getLeft() { return x; }

    /**
     * Gets the Bottom coordinate of the view window
     * @return Bottom coordinate
     */
    public float getBottom() { return y; }

    /**
     * Gets the Right coordinate of the view window
     * @return Right coordinate
     */
    public float getRight() { return x + w; }

    /**
     * Gets width of the view window
     * @return Width of the view window
     */
    public int getWidth() { return w; }

    /**
     * Gets height of the view window
     * @return Height of the view window
     */
    public int getHeight() { return h; }
}
