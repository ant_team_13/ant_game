package ant_project.renderer;

import ant_project.Map;
import ant_project.Simulation;
import ant_project.renderer.CameraView;
import ant_project.renderer.MapRenderer;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


/**
 * Class implementing UI element for displaying the game simulation
 */
public class GameCanvas  implements ApplicationListener {

    private SpriteBatch batch;
    private MapRenderer mr;
    private CameraView cv;
    private Viewport viewport;
    private Map m;
    private OrthographicCamera camera;
    private Simulation s;
    private int stepSize;
    private float elapsedSteps;
    private String redBrainFile;
    private String blackBrainFile;
    private String mapFile;
    private Thread simulationSlave;

    /**
     * Constructs the game canvas from automaton and map files
     * @param redBrainFile File of ant brain for red team
     * @param blackBrainFile File of ant brain for black team
     * @param mapFile File of map to play on
     */
    public GameCanvas(String redBrainFile, String blackBrainFile, String mapFile)
    {
        super();
        this.redBrainFile = redBrainFile;
        this.blackBrainFile = blackBrainFile;
        this.mapFile = mapFile;
        this.s = new Simulation(mapFile, redBrainFile, blackBrainFile);
        this.m = this.s.getMap();
        simulationSlave = new Thread(new SimulationSlave());
        stepSize = 256;
    }

    /**
     * Slave to run the simulation in background while the canvas is drawing
     */
    private class SimulationSlave implements Runnable {
        public void run() {
            int step = (int)elapsedSteps;
            elapsedSteps -= step;
            s.runN(step);
        }
    }

    /**
     * Creates the game canvas from previously provided files
     */
    @Override
    public void create() {

        batch = new SpriteBatch();

        camera = new OrthographicCamera();
        viewport = new ScreenViewport(camera);
        viewport.apply();
        camera.position.set(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2,0);

        cv = new CameraView((6 * m.getWidth() / 2) - Gdx.graphics.getWidth()/2,(4 * m.getWidth() / 2) - Gdx.graphics.getWidth()/2, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        mr = new MapRenderer(6,4,3,batch,m,cv);
    }

    /**
     * Disposes of unnecessary elements
     */
    @Override
    public void dispose() {
    }

    FPSLogger logger = new FPSLogger();

    /**
     * Renders single frame of the game simulation
     */
    @Override
    public void render() {

        logger.log();
        camera.update();
        Gdx.gl20.glClearColor(1, 1, 1, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        float delta = Gdx.graphics.getDeltaTime();
        elapsedSteps += stepSize * Gdx.graphics.getRawDeltaTime();
        cv.update(delta);
        try {
            simulationSlave.join();
        } catch (InterruptedException e) {
            throw new RuntimeException("Unexpected error " + e.getMessage());
        }
        mr.update(delta);
        simulationSlave.run();
        batch.setProjectionMatrix(camera.combined);
        batch.enableBlending();
        batch.begin();
        mr.render();
        batch.end();
    }

    /**
     * Resizes the game canvas
     * @param width New width of the game canvas
     * @param height New height of the game canvas
     */
    @Override
    public void resize(int width, int height) {
        viewport.update(width,height);
        cv.setWB(width,height);
        mr = new MapRenderer(6,4,3,batch,m,cv);
        camera.position.set(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2,0);
    }

    /**
     * Pauses the game canvas
     */
    @Override
    public void pause() {
    }

    /**
     * Resumes the game canvas
     */
    @Override
    public void resume() {
    }

    /**
     * Doubles the speed of the simulation
     */
    public void faster() {
        if (stepSize*2 < Simulation.NUM_TICKS) {stepSize *= 2;}
    }

    /**
     * Halves the speed of the simulation
     */
    public void slower() {
        if (stepSize > 1) { stepSize /= 2; }
    }

    /**
     * Outputs some parameters about the canvas
     * @return String with canvas parameters
     */
    public String toString() {
        return "Speed: " + Integer.toString(stepSize) + " ticks per second. Progress: "
                + Integer.toString(s.getCurrentTick()) + " out of " +  Integer.toString(Simulation.NUM_TICKS) + ".";
    }

    /**
     * Restarts the game simulation
     */
    public void restart() {
        this.s = new Simulation(mapFile, redBrainFile, blackBrainFile);
        this.m = this.s.getMap();
        simulationSlave = new Thread(new SimulationSlave());
        stepSize = 256;
        this.mr.setMap(this.m);
    }

    /**
     * Gets the game simulation of the canvas
     * @return Game Simulation
     */
    public Simulation getSimulation() { return s; }
}
