/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;

/**
 * Class implementing the 'Turn' FSA state.
 */
public class StateTurn implements State {
    private short st;
    private Lr lr;

    /**
     * Constructs the state from arguments provided (in order in which they should be inputted)
     * @param lrStr lr = Left,Right
     * @param stStr st = 0,1,...,9999
     */
    public StateTurn(String lrStr, String stStr) {
        try {
            lr = Lr.fromStr(lrStr);
        } catch (IllegalArgumentException e){
            throw new IllegalArgumentException("State Turn experienced error during construction on first parameter " +
                    "- " + e.getMessage());
        }
        try {
            st = Short.parseShort(stStr);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Turn experienced error during construction - " +
                    "second parameter (state) expected to be integer but \"" + stStr + "\" was found instead");
        }
    }

    /**
     * Executes the logic of state on ant on map provided.
     * @param ant Ant for which the logic should be executed (must be alive).
     * @param map Map on which the game is played.
     */
    @Override
    public void execute(Ant ant, Map map) {
        if (lr == Lr.LEFT) { ant.getDirection().rotateLeft(); }
        else if (lr == Lr.RIGHT) { ant.getDirection().rotateRight(); }
        ant.setState(st);
    }

    /**
     * Validates that the transitions to other states are correct.
     * @param numStates Number of states in the FSA.
     * @return True if the states are valid, false otherwise.
     */
    @Override
    public boolean validate(short numStates) {
        return st >= 0 && st < numStates;
    }

    /**
     * Returns Transition State ID
     * @return Transition State ID
     */
    short getSt() {return this.st;}

    /**
     * Returns Turn Direction
     * @return Turn Direction
     */
    Lr getLr() {return this.lr;}

    /**
     * Compares two States
     * @param ss1 First state for comparison
     * @param ss2 Second state for comparison
     * @return Returns true if states are equal, false otherwise
     */
    public static boolean equals(State ss1, StateTurn ss2) {
        return ss1 instanceof StateTurn
                && (((StateTurn) ss1).getSt() == ss2.getSt())
                & (((StateTurn) ss1).getLr() == ss2.getLr());
    }

}
