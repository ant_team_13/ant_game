/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;
import ant_project.MapCell;


/**
 * Class implementing the 'Sense' FSA state.
 */
public class StateSense implements State {

    private SenseDir sensedir;
    private short st1;
    private short st2;
    private Cond cond;
    private char condAux;

    /**
     * Constructs the state from arguments provided (in order in which they should be inputted)
     * @param sensedirStr
     * @param st1Str st1 = 0,1,...,9999
     * @param st2Str st2 = 0,1,...,9999
     * @param condStr See State.Cond enumeration for valid values.
     * @param condAuxStr If condStr is 'Marker', this parameter has to be 0,1,...,5, provide "" otherwise.
     */
    public StateSense(String sensedirStr, String st1Str, String st2Str, String condStr, String condAuxStr) {
        try {
            sensedir = SenseDir.fromStr(sensedirStr);
        } catch (IllegalArgumentException e){
            throw new IllegalArgumentException("State Sense experienced error during construction on first parameter " +
                    "- " + e.getMessage());
        }
        try {
            cond = Cond.fromStr(condStr);
        } catch (IllegalArgumentException e){
            throw new IllegalArgumentException("State Sense experienced error during construction on fourth " +
                    "parameter - " + e.getMessage());
        }

        try {
            st1 = Short.parseShort(st1Str);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Sense experienced error during construction - " +
                    "second parameter (state) expected to be integer but \"" + st1Str + "\" was found instead");
        }
        try {
            st2 = Short.parseShort(st2Str);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Sense experienced error during construction - " +
                    "third parameter (state) expected to be integer but \"" + st2Str + "\" was found instead");
        }
        try {
            if (!condAuxStr.equals("")) {
                if (cond != Cond.MARKER) {
                    throw new IllegalArgumentException("State Sense experienced error during construction - " +
                            "only four parameters are necessary when condition is not Marker");
                }
                int condAuxTmp = Integer.parseInt(condAuxStr);
                if (condAuxTmp > 5 || condAuxTmp < 0) {
                    throw new IllegalArgumentException("State Sense experienced error during construction - " +
                            "fifth parameter (Marker ID) expected to be in range 0,1,...,5 but \"" + condAuxStr +
                            "\" was found instead");
                }
                condAux = (char) condAuxTmp;
            }
            else {
                if (cond == Cond.MARKER) {
                    throw new IllegalArgumentException("State Sense experienced error during construction - " +
                            "fifth parameter must be provided for Marker condition");
                }
            }
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Sense experienced error during construction - " +
                    "fifth parameter (Marker ID) expected to be integer but \"" + condAuxStr + "\" was found instead");
        }
    }

    private MapCell cellFromSensedir(Ant ant, Map map) {
        switch (sensedir) {
            case HERE: return ant.getPosition();
            case AHEAD: return map.getMapCellInDir(ant.getPosition(), ant.getDirection());
            case LEFT_AHEAD: return map.getMapCellInDir(ant.getPosition(), ant.getDirection().getLeft());
            case RIGHT_AHEAD: return map.getMapCellInDir(ant.getPosition(), ant.getDirection().getRight());
        }
        return null;
    }

    private boolean isCondSatisfied(Ant ant, Map map) {
        MapCell mc = cellFromSensedir(ant, map);
        if (mc == null) { return cond == Cond.ROCK; }
        switch (cond) {
            case FRIEND:
                return mc.containsAnt(ant.getColor());
            case FOE:
                return mc.containsAnt(ant.getColor().opposite());
            case FRIEND_WITH_FOOD:
                return mc.containsAnt(ant.getColor()) && mc.getAnt().hasFood();
            case FOE_WITH_FOOD:
                return mc.containsAnt(ant.getColor().opposite()) && mc.getAnt().hasFood();
            case FOOD:
                return mc.getFoodAmount() > 0;
            case ROCK:
                return mc.isRocky();
            case MARKER:
                return mc.checkMarker(ant.getColor(), condAux);
            case FOE_MARKER:
                return mc.checkMarker(ant.getColor().opposite());
            case HOME:
                return mc.isAnthill(ant.getColor());
            case FOE_HOME:
                return mc.isAnthill(ant.getColor().opposite());
        }
        return false;
    }

    /**
     * Executes the logic of state on ant on map provided.
     * @param ant Ant for which the logic should be executed (must be alive).
     * @param map Map on which the game is played.
     */
    @Override
    public void execute(Ant ant, Map map) {
        if(isCondSatisfied(ant, map)) {
            ant.setState(st1);
        } else {
            ant.setState(st2);
        }
    }

    /**
     * Validates that the transitions to other states are correct.
     * @param numStates Number of states in the FSA.
     * @return True if the states are valid, false otherwise.
     */
    @Override
    public boolean validate(short numStates) {
        return (st1 >= 0 && st1 < numStates && st2 >= 0 && st2 < numStates);
    }

    public SenseDir getSensedir() {return this.sensedir;}

    /**
     * Returns first Transition State ID
     * @return First Transition State ID
     */
    short getSt1() {return this.st1;}

    /**
     * Returns second Transition State ID
     * @return Second Transition State ID
     */
    short getSt2() {return this.st2;}

    /**
     * Returns Condition
     * @return Condition
     */
    Cond getCond() {return this.cond;}

    /**
     * Returns Condition Auxiliary parameter
     * @return Condition Auxiliary parameter
     */
    char getCondAux() {return this.condAux;}

    /**
     * Compares two States
     * @param ss1 First state for comparison
     * @param ss2 Second state for comparison
     * @return Returns true if states are equal, false otherwise
     */
    public static boolean equals(State ss1, StateSense ss2) {
        return ss1 instanceof StateSense
                && (((StateSense) ss1).getSensedir().equals(ss2.getSensedir()))
                & (((StateSense) ss1).getSt1() == ss2.getSt1()) & (((StateSense) ss1).getSt2() == ss2.getSt2())
                & (((StateSense) ss1).getCond().equals(ss2.getCond()))
                & (((StateSense) ss1).getCondAux() == ss2.getCondAux());
    }

}
