package ant_project.fsa;

import ant_project.fsa.State;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SenseDirTest {

    @Test
    public void testFromStr() throws Exception {
        assertEquals(State.SenseDir.fromStr("Here"), State.SenseDir.HERE);
        assertEquals(State.SenseDir.fromStr("Ahead"), State.SenseDir.AHEAD);
        assertEquals(State.SenseDir.fromStr("LeftAhead"), State.SenseDir.LEFT_AHEAD);
        assertEquals(State.SenseDir.fromStr("RightAhead"), State.SenseDir.RIGHT_AHEAD);

        //Rejection tests
        {boolean exc = false; try { State.SenseDir.fromStr("here"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("HERE"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("ahead"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("AHEAD"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("leftahead"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("LEFTAHEAD"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("rightahead"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("RIGHTAHEAD"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr(""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("Herea"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.SenseDir.fromStr("Her"); } catch (Exception e) {exc = true;} assert(exc);}
    }
}