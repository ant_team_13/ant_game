package ant_project.fsa;

import ant_project.fsa.State;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LrTest {

    @Test
    public void testFromStr() throws Exception {
        assertEquals(State.Lr.fromStr("Left"), State.Lr.LEFT);
        assertEquals(State.Lr.fromStr("Right"), State.Lr.RIGHT);

        //Rejection tests
        {boolean exc = false; try { State.Lr.fromStr("left"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Lr.fromStr("LEFT"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Lr.fromStr("right"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Lr.fromStr("RIGHT"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Lr.fromStr(""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Lr.fromStr("1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Lr.fromStr("Herea"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Lr.fromStr("Her"); } catch (Exception e) {exc = true;} assert(exc);}
    }
}