package ant_project.fsa;

import ant_project.fsa.StateSense;
import org.junit.Test;

import static org.junit.Assert.*;

public class StateSenseTest {

    @Test
    public void testConstructor() throws Exception {
        //Test parameter rejection
        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Home", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("LeftAhead", "1", "1", "FriendWithFood", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("RightAhead", "1", "1", "Foe", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Rock", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "FoeMarker", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Friend", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9", "11", "FoeWithFood", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "0", "Home", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "9999", "Home", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "9999", "Home", ""); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "0", "Home", ""); } catch (Exception e) {exc = true;} assert(!exc);}

        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Marker", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9", "11", "Marker", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "0", "Marker", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "9999", "Marker", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "9999", "Marker", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "0", "Marker", "0"); } catch (Exception e) {exc = true;} assert(!exc);}

        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Marker", "5"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9", "11", "Marker", "5"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "0", "Marker", "5"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "9999", "Marker", "5"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "9999", "Marker", "5"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "0", "Marker", "5"); } catch (Exception e) {exc = true;} assert(!exc);}

        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Marker", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9", "11", "Marker", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "0", "Marker", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "9999", "Marker", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "9999", "Marker", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "0", "Marker", ""); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Marker", "6"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9", "11", "Marker", "6"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "0", "Marker", "6"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "9999", "Marker", "6"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "9999", "Marker", "6"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "0", "Marker", "6"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Marker", "-1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9", "11", "Marker", "-1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "0", "Marker", "-1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "9999", "Marker", "-1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "0", "9999", "Marker", "-1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "9999", "0", "Marker", "-1"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Marker", "a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahad", "1", "1", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "Marer", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "a", "1", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "a", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", ".", "1", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", ".", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "!", "1", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "!", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "", "1", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("", "1", "1", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("100", "1", "1", "Marker", "5"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateSense("Ahead", "1", "1", "100", "5"); } catch (Exception e) {exc = true;} assert(exc);}
    }

    @Test
    public void testValidate() throws Exception {
        {
            StateSense st = new StateSense("Ahead", "100", "00", "Home", "");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateSense st = new StateSense("Ahead", "100", "1", "Home", "");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateSense st = new StateSense("Ahead", "1", "100", "Home", "");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateSense st = new StateSense("Ahead", "0", "0", "Home", "");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(st.validate((short)1));
        }
        {
            StateSense st = new StateSense("Ahead", "-1", "-1", "Home", "");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
        {
            StateSense st = new StateSense("Ahead", "0", "-1", "Home", "");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
        {
            StateSense st = new StateSense("Ahead", "-1", "0", "Home", "");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
    }
}