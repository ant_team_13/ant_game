/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;

/**
 * Class implementing the 'PickUp' FSA state.
 */
public class StatePickup implements State {
    private short st1;
    private short st2;

    /**
     * Constructs the state from arguments provided (in order in which they should be inputted)
     * @param st1Str st1 = 0,1,...,9999
     * @param st2Str st2 = 0,1,...,9999
     */
    public StatePickup(String st1Str, String st2Str) {
        try {
            st1 = Short.parseShort(st1Str);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Pickup experienced error during construction - " +
                    "first parameter (state) expected to be integer but \"" + st1Str + "\" was found instead");
        }
        try {
            st2 = Short.parseShort(st2Str);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Pickup experienced error during construction - " +
                    "second parameter (state) expected to be integer but \"" + st2Str + "\" was found instead");
        }
    }

    /**
     * Executes the logic of state on ant on map provided.
     * @param ant Ant for which the logic should be executed (must be alive).
     * @param map Map on which the game is played.
     */
    @Override
    public void execute(Ant ant, Map map) {
        if (ant.hasFood() || ant.getPosition().getFoodAmount() == 0) {
            ant.setState(st2);
        } else {
            ant.getPosition().setFoodAmount(ant.getPosition().getFoodAmount() - 1);
            ant.setHasFood(true);
            ant.setState(st1);
        }
    }

    /**
     * Validates that the transitions to other states are correct.
     * @param numStates Number of states in the FSA.
     * @return True if the states are valid, false otherwise.
     */
    @Override
    public boolean validate(short numStates) {
        return (st1 >= 0 && st1 < numStates && st2 >= 0 && st2 < numStates);
    }


    /**
     * Returns first Transition State ID
     * @return First Transition State ID
     */
    short getSt1() {return this.st1;}


    /**
     * Returns second Transition State ID
     * @return Second Transition State ID
     */
    short getSt2() {return this.st2;}

    /**
     * Compares two States
     * @param ss1 First state for comparison
     * @param ss2 Second state for comparison
     * @return Returns true if states are equal, false otherwise
     */
    public static boolean equals(State ss1, StatePickup ss2) {
        return ss1 instanceof StatePickup
                && (((StatePickup) ss1).getSt1() == ss2.getSt1())
                & (((StatePickup) ss1).getSt2() == ss2.getSt2());
    }
}
