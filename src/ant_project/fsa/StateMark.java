/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;

/**
 * Class implementing the 'Mark' FSA state.
 */
public class StateMark implements State {

    private short i;
    private short st;

    /**
     * Constructs the state from arguments provided (in order in which they should be inputted)
     * @param iStr i = 0,1,...,5
     * @param stStr st = 0,1,...,9999
     */
    public StateMark(String iStr, String stStr) {
        try {
            i = Short.parseShort(iStr);
            if(i < 0 || i > 5) {
                throw new IllegalArgumentException("State Mark experienced error during construction - " +
                        "first parameter (Marker ID) expected to be in range 0,1,...,5 but \"" + iStr + "\" was found instead");
            }
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Mark experienced error during construction - " +
                    "first parameter (Marker ID) expected to be integer but \"" + iStr + "\" was found instead");
        }
        try {
            st = Short.parseShort(stStr);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Mark experienced error during construction - " +
                    "second parameter (state) expected to be integer but \"" + stStr + "\" was found instead");
        }
    }

    /**
     * Executes the logic of state on ant on map provided.
     * @param ant Ant for which the logic should be executed (must be alive).
     * @param map Map on which the game is played.
     */
    @Override
    public void execute(Ant ant, Map map) {
        ant.getPosition().setMarker(ant.getColor(), i);
        ant.setState(st);
    }

    /**
     * Validates that the transitions to other states are correct.
     * @param numStates Number of states in the FSA.
     * @return True if the states are valid, false otherwise.
     */
    @Override
    public boolean validate(short numStates) {
        return (st >= 0 && st < numStates);
    }

    /**
     * Returns Transition State ID
     * @return Transition State ID
     */
    short getSt() {return this.st;}

    /**
     * Returns the Marker ID
     * @return Marker ID
     */
    short getI() {return this.i;}

    /**
     * Compares two States
     * @param ss1 First state for comparison
     * @param ss2 Second state for comparison
     * @return Returns true if states are equal, false otherwise
     */
    public static boolean equals(State ss1, StateMark ss2) {
        return ss1 instanceof StateMark
                && (((StateMark) ss1).getSt() == ss2.getSt())
                & (((StateMark) ss1).getI() == ss2.getI());
    }
}
