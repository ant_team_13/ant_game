/**
 * Created by Daniel Saska on 22/02/2015
 */
package ant_project.fsa;

/**
 * Class representing the compiler which transforms code strings into state lists.
 */
public class Compiler {

    public static int currentState;

    /**
     * Method compiling the code string provided into set of states and then returning it.
     * @param rawCodeString The raw code string for the ant brain provided by the player.
     * @return List of states compiled from the input string.
     */
    public static State[] compile(String rawCodeString) {

        String codeString[] = rawCodeString.split("\\r?\\n");

        if(codeString.length > 10000) {
            throw new IllegalArgumentException("Maximum length of script file allowed is 10000, "
                    + Integer.toString(codeString.length) + "found");
        }

        State[] states = new State[codeString.length];
        for(int i = 0;i < codeString.length;i++) {
            //Removes comments from line
            String line = codeString[i];
            int comment = line.indexOf(";");
            if(comment != -1) { line = line.substring(0, comment); } //Remove comment if exists

            currentState = i;

            states[i] = resolveState(line, i+1);
            if (states[i] == null) {
                throw new IllegalArgumentException("Line " + Integer.toString(i+1)
                        + " \"" + line + "\": " + "The line is not valid.");
            }
        }

        if(states.length < 1) {
            throw new IllegalArgumentException("The file contains no valid Ant Brain States, this is not allowed");
        }

        return states;
    }

    /**
     * Takes a line from the Ant Brain File, tokenises it and returns the State that is represented by the line
     * @param line The given line from the Ant Brain File
     * @return The State represented by the given line
     */
    public static State resolveState(String line, int lineN) {

        //Tokenises line
        String[] tokens = line.split(" ");

        State currentState = null;

        try {
            if (tokens.length < 1) {
                throw new IllegalArgumentException("No tokens found");
            }
            if (tokens[0].equals("Sense")) {
                if (tokens.length < 5 || tokens.length > 6) {
                    throw new IllegalArgumentException("Sense needs 4 or 5 arguments, "
                            + Integer.toString(tokens.length - 1) + " found");
                }
                currentState = new StateSense(tokens[1]
                        , tokens[2]
                        , tokens[3]
                        , tokens[4]
                        , ((tokens.length == 5) ? "" : tokens[5]));
            } else if (tokens[0].equals("Mark")) {
                if (tokens.length != 3) {
                    throw new IllegalArgumentException("Mark needs 2 arguments, "
                            + Integer.toString(tokens.length - 1) + " found");
                }
                currentState = new StateMark(tokens[1], tokens[2]);
            } else if (tokens[0].equals("Unmark")) {
                if (tokens.length != 3) {
                    throw new IllegalArgumentException("Unmark needs 2 arguments, "
                            + Integer.toString(tokens.length - 1) + " found");
                }
                currentState = new StateUnmark(tokens[1], tokens[2]);
            } else if (tokens[0].equals("PickUp")) {
                if (tokens.length != 3) {
                    throw new IllegalArgumentException("PickUp needs 2 arguments, "
                            + Integer.toString(tokens.length - 1) + " found");
                }
                currentState = new StatePickup(tokens[1], tokens[2]);
            } else if (tokens[0].equals("Drop")) {
                if (tokens.length != 2) {
                    throw new IllegalArgumentException("Drop needs 1 arguments, "
                            + Integer.toString(tokens.length - 1) + " found");
                }
                currentState = new StateDrop(tokens[1]);
            } else if (tokens[0].equals("Turn")) {
                if (tokens.length != 3) {
                    throw new IllegalArgumentException("Turn needs 2 arguments, "
                            + Integer.toString(tokens.length - 1) + " found");
                }
                currentState = new StateTurn(tokens[1], tokens[2]);
            } else if (tokens[0].equals("Move")) {
                if (tokens.length != 3) {
                    throw new IllegalArgumentException("Move needs 2 arguments, "
                            + Integer.toString(tokens.length - 1) + " found");
                }
                currentState = new StateMove(tokens[1], tokens[2]);
            } else if (tokens[0].equals("Flip")) {
                if (tokens.length != 4) {
                    throw new IllegalArgumentException("Flip needs 3 arguments, "
                            + Integer.toString(tokens.length - 1) + " found");
                }
                currentState = new StateFlip(tokens[1], tokens[2], tokens[3]);
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Line " + Integer.toString(lineN)
                    + " \"" + line + "\": " + e.getMessage());
        }
        return currentState;
    }
}