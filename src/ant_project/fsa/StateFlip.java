/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;
import ant_project.Rng;

/**
 * Class implementing the 'Flip' FSA state.
 */
public class StateFlip implements State{
    private short st1;
    private short st2;
    private int p;

    /**
     * Constructs the state from arguments provided (in order in which they should be inputted)
     * @param pStr p = 1,2,3...
     * @param st1Str st1 = 0,1,...,9999
     * @param st2Str st2 = 0,1,...,9999
     */
    public StateFlip(String pStr, String st1Str, String st2Str) {
        try {
            st1 = Short.parseShort(st1Str);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Flip experienced error during construction - " +
                    "second parameter (state) expected to be integer but \"" + st1Str + "\" was found instead");
        }
        try {
            st2 = Short.parseShort(st2Str);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Flip experienced error during construction - " +
                    "third parameter (state) expected to be integer but \"" + st2Str + "\" was found instead");
        }
        try {
            p = Integer.parseInt(pStr);
            if (p < 1) {
                throw new IllegalArgumentException("State Flip experienced error during construction - " +
                        "first parameter (random chance) expected to be positive but \"" + pStr + "\" was found instead");
            }
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Flip experienced error during construction - " +
                    "first parameter (random chance) expected to be integer but \"" + pStr + "\" was found instead");
        }
    }

    /**
     * Executes the logic of state on ant on map provided.
     * @param ant Ant for which the logic should be executed (must be alive).
     * @param map Map on which the game is played.
     */
    @Override
    public void execute(Ant ant, Map map) {
        if (Rng.randomInt(p) == 0) {
            ant.setState(st1);
        } else {
            ant.setState(st2);
        }
    }

    /**
     * Validates that the transitions to other states are correct.
     * @param numStates Number of states in the FSA.
     * @return True if the states are valid, false otherwise.
     */
    @Override
    public boolean validate(short numStates) {
        return (st1 >= 0 && st1 < numStates && st2 >= 0 && st2 < numStates);
    }

    /**
     * Returns first Transition State ID
     * @return First Transition State ID
     */
    public short getSt1() {return this.st1;}

    /**
     * Returns second Transition State ID
     * @return second Transition State ID
     */
    public short getSt2() {return this.st2;}

    /**
     * Returns Random chance
     * @return Random chance
     */
    public int getP() {return this.p;}


    /**
     * Compares two States
     * @param ss1 First state for comparison
     * @param ss2 Second state for comparison
     * @return Returns true if states are equal, false otherwise
     */
    public static boolean equals(State ss1, StateFlip ss2) {
        return ss1 instanceof StateFlip
                && (((StateFlip) ss1).getSt1() == ss2.getSt1())
                & (((StateFlip) ss1).getSt2() == ss2.getSt2())
                & (((StateFlip) ss1).getP() == ss2.getP());
    }
}
