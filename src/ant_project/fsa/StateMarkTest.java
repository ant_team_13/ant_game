package ant_project.fsa;

import ant_project.fsa.StateMark;
import org.junit.Test;

import static org.junit.Assert.*;

public class StateMarkTest {

    @Test
    public void testConstructor() throws Exception {
        //Test parameter rejection
        {boolean exc = false; try { new StateMark("0", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMark("5", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMark("1", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMark("2", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMark("6", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("-1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("a", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark(".", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("!", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("1", "a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("1", "."); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("1", "!"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("1", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("-100", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMark("-1000000", "1"); } catch (Exception e) {exc = true;} assert(exc);}
    }

    @Test
    public void testValidate() throws Exception {
        {
            StateMark st = new StateMark("0","100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateMark st = new StateMark("1", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(st.validate((short)1));
        }
        {
            StateMark st = new StateMark("1", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
    }

}