/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;
import ant_project.MapCell;

/**
 * Class implementing the 'Move' FSA state.
 */
public class StateMove implements State {
    private short st1;
    private short st2;

    /**
     * Constructs the state from arguments provided (in order in which they should be inputted)
     * @param st1Str st1 = 0,1,...,9999
     * @param st2Str st2 = 0,1,...,9999
     */
    public StateMove(String st1Str, String st2Str) {
        try {
            st1 = Short.parseShort(st1Str);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Move experienced error during construction - " +
                    "first parameter (state) expected to be integer but \"" + st1Str + "\" was found instead");
        }
        try {
            st2 = Short.parseShort(st2Str);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("State Move experienced error during construction - " +
                    "second parameter (state) expected to be integer but \"" + st2Str + "\" was found instead");
        }
    }

    /**
     * Executes the logic of state on ant on map provided.
     * @param ant Ant for which the logic should be executed (must be alive).
     * @param map Map on which the game is played.
     */
    @Override
    public void execute(Ant ant, Map map) {
        MapCell mc = map.getMapCellInDir(ant.getPosition(), ant.getDirection());
        if(mc == null || mc.isRocky() || mc.containsAnt()) {
            ant.setState(st2);
        } else {
            ant.getPosition().clearAnt();
            ant.setPosition(map.getMapCellInDir(ant.getPosition(), ant.getDirection()));
            ant.getPosition().setAnt(ant);
            ant.setState(st1);
            ant.setResting(14);

            //Whenever ant moves, possibility of ants being killed has to be considered on this and all surrounding
            //cells
            char numAdj = map.getAdjacentAnts(ant.getPosition(), ant.getColor().opposite());
            if (numAdj > 4) { ant.kill(); return;} //Kill this ant
            //Check surrounding positions for enemy ants
            for (int i = 0; i < 6; ++i) {
                MapCell mca = map.getMapCellInDir(ant.getPosition(), Ant.Direction.fromInt(i));
                if (mca != null && mca.containsAnt() && mca.getAnt().getColor() == ant.getColor().opposite()) {
                    numAdj = map.getAdjacentAnts(mca, ant.getColor());
                    if (numAdj > 4) { mca.getAnt().kill(); } //Kill this ant
                }
            }
        }
    }

    /**
     * Validates that the transitions to other states are correct.
     * @param numStates Number of states in the FSA.
     * @return True if the states are valid, false otherwise.
     */
    @Override
    public boolean validate(short numStates) {
        return (st1 >= 0 && st1 < numStates && st2 >= 0 && st2 < numStates);
    }

    /**
     * Returns first Transition State ID
     * @return First Transition State ID
     */
    short getSt1() {return this.st1;}

    /**
     * Returns second Transition State ID
     * @return second Transition State ID
     */
    short getSt2() {return this.st2;}


    /**
     * Compares two States
     * @param ss1 First state for comparison
     * @param ss2 Second state for comparison
     * @return Returns true if states are equal, false otherwise
     */
    public static boolean equals(State ss1, StateMove ss2) {
        return ss1 instanceof StateMove
                && (((StateMove) ss1).getSt1() == ss2.getSt1())
                & (((StateMove) ss1).getSt2() == ss2.getSt2());
    }
}
