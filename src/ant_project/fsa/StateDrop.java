/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;

/**
 * Drop’ type state shall cause current ant to drop food (if the ant has any) on
 * the current map cell. If the ant has food, food of the current map cell shall
 * be incremented and ant’s parameter shall be set to indicate that the ant is
 * not in possession of food. State counter shall be set to State ID specified
 * in first argument token.
 */
/**
 * Class implementing the 'Drop' FSA state.
 */
public class StateDrop implements State {

    private short st;

    /**
     * Constructs the state from arguments provided (in order in which they
     * should be inputted)
     *
     * @param stStr st = 0,1,...,9999
     */
    public StateDrop(String stStr) {
        try {
            st = Short.parseShort(stStr);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("State Drop experienced error during construction - " +
                    "first parameter (state) expected to be integer but \"" + stStr + "\" was found instead");
        }
    }

    /**
     * Executes the logic of state on ant on map provided.
     *
     * @param ant Ant for which the logic should be executed (must be alive).
     * @param map Map on which the game is played.
     */
    @Override
    public void execute(Ant ant, Map map) {

        if (ant.hasFood()) {
            ant.getPosition().setFoodAmount(ant.getPosition().getFoodAmount() + 1);
            ant.setHasFood(false);
        }
        ant.setState(st);
    }

    /**
     * Validates that the transitions to other states are correct.
     *
     * @param numStates Number of states in the FSA.
     * @return True if the states are valid, false otherwise.
     */
    @Override
    public boolean validate(short numStates) {
        return st >= 0 && st < numStates;
    }

    /**
     * Returns Transition State ID
     * @return Transition State ID
     */
    public short getSt() {return this.st;}

    /**
     * Compares two States
     * @param ss1 First state for comparison
     * @param ss2 Second state for comparison
     * @return Returns true if states are equal, false otherwise
     */
    public static boolean equals(State ss1, StateDrop ss2) {
        return ss1 instanceof StateDrop
                && (((StateDrop) ss1).getSt() == ss2.getSt());
    }
}
