package ant_project.fsa;

import ant_project.fsa.StateDrop;
import org.junit.Test;

import static org.junit.Assert.*;

public class StateDropTest {

    @Test
    public void testConstructor() throws Exception {
        //Test parameter rejection
        {boolean exc = false; try { new StateDrop("100"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateDrop("0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateDrop("9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateDrop("a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateDrop("."); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateDrop("!"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateDrop(""); } catch (Exception e) {exc = true;} assert(exc);}
    }

    @Test
    public void testValidate() throws Exception {
        {
            StateDrop st = new StateDrop("100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateDrop st = new StateDrop("0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(st.validate((short)1));
        }
        {
            StateDrop st = new StateDrop("-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
    }
}