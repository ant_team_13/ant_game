package ant_project.fsa;

import ant_project.fsa.StateFlip;
import org.junit.Test;

import static org.junit.Assert.*;

public class StateFlipTest {

    @Test
    public void testConstructor() throws Exception {
        //Test parameter rejection
        {boolean exc = false; try { new StateFlip("100", "1", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateFlip("1", "1", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateFlip("1", "0", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateFlip("1", "9999", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateFlip("1", "0", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateFlip("1", "9999", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateFlip("9999999", "1", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateFlip("a", "1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip(".", "1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("!", "1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("1", "a", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("1", ".", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("1", "!", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("1", "1", "a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("1", "1", "!"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("1", "1", "."); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("", "1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("1", "", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("1", "1", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("-100", "1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("-1000000", "1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateFlip("0", "1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
    }

    @Test
    public void testValidate() throws Exception {
        {
            StateFlip st = new StateFlip("100", "100", "100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateFlip st = new StateFlip("100", "0", "100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateFlip st = new StateFlip("100", "100", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateFlip st = new StateFlip("1", "0", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(st.validate((short)1));
        }
        {
            StateFlip st = new StateFlip("1", "-1", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
        {
            StateFlip st = new StateFlip("1", "0", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
        {
            StateFlip st = new StateFlip("1", "-1", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
    }
}