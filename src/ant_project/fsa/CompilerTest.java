package ant_project.fsa;

import ant_project.fsa.*;
import ant_project.fsa.Compiler;
import junit.framework.TestCase;

public class CompilerTest extends TestCase {

    public void testCompile() throws Exception {

        State[] testStates = ant_project.fsa.Compiler.compile(
                "Sense Ahead 1 3 Food  ; state 0:  [SEARCH] is there food in front of me?\n" +
                        "Move 2 0              ; state 1:  YES: move onto food (return to state 0 on failure)\n" +
                        "PickUp 8 0            ; state 2:       pick up food and jump to state 8 (or 0 on failure)\n" +
                        "Flip 3 4 5            ; state 3:  NO: choose whether to...\n" +
                        "Turn Left 0           ; state 4:      turn left and return to state 0\n" +
                        "Flip 2 6 7            ; state 5:      ... or ...\n" +
                        "Turn Right 0          ; state 6:      turn right and return to state 0\n" +
                        "Move 0 3              ; state 7:      ... or move forward and return to state 0 (or 3 on failure)\n" +
                        "Sense Ahead 9 11 Home ; state 8:  [GO HOME] is the cell in front of me my anthill?\n" +
                        "Move 10 8             ; state 9:  YES: move onto anthill\n" +
                        "Drop 0                ; state 10:     drop food and return to searching\n" +
                        "Flip 3 12 13          ; state 11: NO: choose whether to...\n" +
                        "Turn Left 8           ; state 12:     turn left and return to state 8\n" +
                        "Flip 2 14 15          ; state 13:     ...or...\n" +
                        "Turn Right 8          ; state 14:     turn right and return to state 8\n" +
                        "Move 8 11             ; state 15:     ...or move forward and return to state 8\n" +
                        "Mark 2 17             ; state 16:     descr\n" +
                        "Unmark 2 0            ; state 17:     descr");

        assert(StateSense.equals(testStates[0], new StateSense("Ahead", "1", "3", "Food", "")));
        assert(StateMove.equals(testStates[1], new StateMove("2", "0")));
        assert(StatePickup.equals(testStates[2], new StatePickup("8", "0")));
        assert(StateFlip.equals(testStates[3], new StateFlip("3", "4", "5")));
        assert(StateTurn.equals(testStates[4], new StateTurn("Left", "0")));
        assert(StateFlip.equals(testStates[5], new StateFlip("2", "6", "7")));
        assert(StateTurn.equals(testStates[6], new StateTurn("Right", "0")));
        assert(StateMove.equals(testStates[7], new StateMove("0", "3")));
        assert(StateSense.equals(testStates[8], new StateSense("Ahead", "9", "11", "Home", "")));
        assert(StateMove.equals(testStates[9], new StateMove("10", "8")));
        assert(StateDrop.equals(testStates[10], new StateDrop("0")));
        assert(StateFlip.equals(testStates[11], new StateFlip("3", "12", "13")));
        assert(StateTurn.equals(testStates[12], new StateTurn("Left", "8")));
        assert(StateFlip.equals(testStates[13], new StateFlip("2", "14", "15")));
        assert(StateTurn.equals(testStates[14], new StateTurn("Right", "8")));
        assert(StateMove.equals(testStates[15], new StateMove("8", "11")));
        assert(StateMark.equals(testStates[16], new StateMark("2", "17")));
        assert(StateUnmark.equals(testStates[17], new StateUnmark("2", "0")));


        //Test code rejection
        {boolean exc = false; try { Compiler.compile("Drop 0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Drop 0\n"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Drop 10"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Drop 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Drop 9999 9"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Drop"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile(""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("\n"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("\n\n"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Dr\nop"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Drop 1 1 1"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { Compiler.compile("Flip 3 4 5"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Flip 3 4 5\n"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Flip 3 9999 5"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Flip 3 9999 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Flip 3 1 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Flip"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Flip 3"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Flip 3 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Flip 3 1 1 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Flip\n3 9999 5"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { Compiler.compile("Mark 2 17"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Mark 2 17\n"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Mark 0 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Mark 3 0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Mark"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Mark 0"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Mark 0 100 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Mark 0 100 1 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Mark 0 100 a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Mark\n 0 100 a"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { Compiler.compile("Move 10 8"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Move 10 8\n"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Move 9999 8"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Move 1 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Move 9999 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Move 0 0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Move"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Move 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Move 1 1 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Move 1 1 a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Move 1 1 1 1"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { Compiler.compile("PickUp 8 0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("PickUp 8 0\n"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("PickUp 9999 0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("PickUp 2 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("PickUp 9999 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("PickUp"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("PickUp 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("PickUp 1 1 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("PickUp 1 1 az"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("PickUp 1 1 1 1"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { Compiler.compile("Sense Ahead 1 3 Food"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 1 3 Food\n"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 9999 3 Food"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 9999 9999 Food"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 1 9999 Food"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 2 3 Marker 1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 8 3 Marker 0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 6 3 Marker 5"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 6 3 Marker"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 6 3 Marker 1 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 6 3"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 6"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Sense Ahead 1 3 Food 1"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { Compiler.compile("Turn Left 0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Turn Left 0\n"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Turn Left 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Turn Left 10"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Turn Left 10 1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Turn Left"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Turn"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Turn Left 10 1 1"); } catch (Exception e) {exc = true;} assert(exc);}

        {boolean exc = false; try { Compiler.compile("Unmark 3 0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Unmark 3 0\n"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Unmark 3 9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { Compiler.compile("Unmark 3"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Unmark"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Unmark 3 3 3"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { Compiler.compile("Unmark 3 3 3 3"); } catch (Exception e) {exc = true;} assert(exc);}

    }
}