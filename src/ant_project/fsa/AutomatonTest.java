package ant_project.fsa;

import ant_project.Ant;
import ant_project.MapCell;
import ant_project.fsa.Automaton;
import junit.framework.TestCase;

public class AutomatonTest extends TestCase {

    public void testConstructor() throws Exception {
        {
            Automaton testAutomaton = new Automaton(
                    "Sense Ahead 1 3 Food  ; state 0:  [SEARCH] is there food in front of me?\n" +
                            "Move 2 0              ; state 1:  YES: move onto food (return to state 0 on failure)\n" +
                            "PickUp 8 0            ; state 2:       pick up food and jump to state 8 (or 0 on failure)\n" +
                            "Flip 3 4 5            ; state 3:  NO: choose whether to...\n" +
                            "Turn Left 0           ; state 4:      turn left and return to state 0\n" +
                            "Flip 2 6 7            ; state 5:      ... or ...\n" +
                            "Turn Right 0          ; state 6:      turn right and return to state 0\n" +
                            "Move 0 3              ; state 7:      ... or move forward and return to state 0 (or 3 on failure)\n" +
                            "Sense Ahead 9 11 Home ; state 8:  [GO HOME] is the cell in front of me my anthill?\n" +
                            "Move 10 8             ; state 9:  YES: move onto anthill\n" +
                            "Drop 0                ; state 10:     drop food and return to searching\n" +
                            "Flip 3 12 13          ; state 11: NO: choose whether to...\n" +
                            "Turn Left 8           ; state 12:     turn left and return to state 8\n" +
                            "Flip 2 14 15          ; state 13:     ...or...\n" +
                            "Turn Right 8          ; state 14:     turn right and return to state 8\n" +
                            "Move 8 11             ; state 15:     ...or move forward and return to state 8\n" +
                            "Mark 2 17             ; state 16:     descr\n" +
                            "Unmark 2 0            ; state 17:     descr");
        }
        {
            Automaton testAutomaton = new Automaton(
                    "Sense Ahead 1 3 Food  ; state 0:  [SEARCH] is there food in front of me?\n" +
                            "Move 2 0 \n" +
                            "PickUp 8 0  \n" +
                            "Flip 3 4 5  \n" +
                            "Turn Left 0  \n" +
                            "Flip 2 6 7\n" +
                            "Turn Right 0          ;\n" +
                            "Move 0 3\n" +
                            "Sense Ahead 9 11 Home\n" +
                            "Move 10 8\n" +
                            "Drop 0\n" +
                            "Flip 3 12 13\n" +
                            "Turn Left 8       \n" +
                            "Flip 2 14 15          ; state 13:     ...or...\n" +
                            "Turn Right 8\n" +
                            "Move 8 11\n" +
                            "Mark 2 17 \n" +
                            "Unmark 2 0 \n");
        }

        {
            boolean exc = false;
            try {
                Automaton testAutomaton = new Automaton(
                        "Sense Ahead 1 3 Food  ; state 0:  [SEARCH] is there food in front of me?\n" +
                                "Move 2 0 \n" +
                                "PickUp 8 0  \n" +
                                "Flip 3 4 5  \n" +
                                "Turn Left 0  \n" +
                                "Flip 2 6 7\n" +
                                "Turn Right 0          ;\n" +
                                "Move 0 3\n" +
                                "Sense Ahead 9 11 Home\n" +
                                "Move 10 8\n" +
                                "Drop 0\n" +
                                "Flip 3 12 13\n" +
                                "Turn Left 8       \n" +
                                "Flip 2 14 15          ; state 13:     ...or...\n" +
                                "Turn Right 8\n" +
                                "Move 8 11\n" +
                                "Mark 2 100 \n" +
                                "Unmark 2 0 \n");
            } catch (Exception e) {
                exc = true;
            }
            assert(exc);
        }

        {
            boolean exc = false;
            try {
                Automaton testAutomaton = new Automaton(
                        "Sense Ahead 1 3 Food  ; state 0:  [SEARCH] is there food in front of me?\n" +
                                "Move 2 0 \n" +
                                "PickUp 8 0  \n" +
                                "Flip 3 4 5  \n" +
                                "Turn Left 0  \n" +
                                "Flip 2 6 7\n" +
                                "Turn Right 0          ;\n" +
                                "Move 0 3\n" +
                                "Sense Ahead 9 11 Home\n" +
                                "Move 10 8\n" +
                                "Drop 0\n" +
                                "Flip 3 12 13\n" +
                                "Turn Left 8       \n" +
                                "Flip 2 14 15          ; state 13:     ...or...\n" +
                                "Turn Right 8\n" +
                                "Move 8 11\n" +
                                "Mark 2 -1 \n" +
                                "Unmark 2 0 \n");
            } catch (Exception e) {
                exc = true;
            }
            assert(exc);
        }

    }

    public void testExecute() throws Exception {
        Automaton testAutomaton = new Automaton("Flip 1 1 1\nFlip 1 2 2\nFlip 1 3 3\nFlip 1 0 0");
        Ant testAnt = new Ant(Ant.TeamColor.BLACK);
        assertEquals(testAnt.getState(), 0);
        for (int i = 1; i <= 100; ++i) {
            testAutomaton.execute(testAnt, null);
            assertEquals(testAnt.getState(), i % 4);
        }
    }

}