package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;

/**
 * Created by Daniel Saska on 22/02/2015.
 */

public interface State {

    /**
     *  Enumeration representing Sense Direction
     */
    enum SenseDir {

        HERE, AHEAD, LEFT_AHEAD, RIGHT_AHEAD;

        /**
         * Receives case sensitive keyword for sense direction and translates into an enum type
         * @param sensedirStr string or 'token' for sense direction
         * @return appropriate sense direction of type enum
         */
        public static SenseDir fromStr(String sensedirStr) {
            if (sensedirStr.equals("Here")) { return HERE; }
            if (sensedirStr.equals("Ahead")) { return AHEAD; }
            if (sensedirStr.equals("LeftAhead")) { return LEFT_AHEAD; }
            if (sensedirStr.equals("RightAhead")) { return RIGHT_AHEAD; }
            throw new IllegalArgumentException("Sense Direction could not be parsed from disallowed string \""
                    + sensedirStr + "\"");
        }
    }

    /**
     *  Enumeration representing Condition
     */
    enum Cond {
        FRIEND, FOE, FRIEND_WITH_FOOD, FOE_WITH_FOOD, FOOD, ROCK, MARKER, FOE_MARKER, HOME, FOE_HOME;

        /**
         * Receives case sensitive keyword for condition of ant and translates into an enum type
         * @param condStr string or 'token' for condition
         * @return appropriate condition of type enum
         */
        public static Cond fromStr(String condStr) {
            if (condStr.equals("Friend")) { return FRIEND; }
            if (condStr.equals("Foe")) { return FOE; }
            if (condStr.equals("FriendWithFood")) { return FRIEND_WITH_FOOD; }
            if (condStr.equals("FoeWithFood")) { return FOE_WITH_FOOD; }
            if (condStr.equals("Food")) { return FOOD; }
            if (condStr.equals("Rock")) { return ROCK; }
            if (condStr.equals("Marker")) { return MARKER; }
            if (condStr.equals("FoeMarker")) { return FOE_MARKER; }
            if (condStr.equals("Home")) { return HOME; }
            if (condStr.equals("FoeHome")) { return FOE_HOME; }
            throw new IllegalArgumentException("Condition could not be parsed from disallowed string \""
                    + condStr + "\"");
        }
    }

    /**
     * Enumeration representing Turning Argument
     */
    enum Lr {

        LEFT, RIGHT;

        /**
         * Receives case sensitive keyword for turn argument and translates into an enum type
         * @param lrStr string or 'token' for which way to turn
         * @return appropriate direction to turn of type enum
         */
        public static Lr fromStr(String lrStr) {
            if (lrStr.equals("Left")) { return LEFT; }
            if (lrStr.equals("Right")) { return RIGHT; }
            throw new IllegalArgumentException("Turning Direction could not be parsed from disallowed string \""
                    + lrStr + "\"");
        }
    }

    /**
     * Executes the logic of state on ant on map provided.
     *
     * @param ant Ant for which the logic should be executed (must be alive).
     * @param map Map on which the game is played.
     */
    void execute(Ant ant, Map map);

    /**
     * Validates that the transitions to other states are correct.
     *
     * @param numStates Number of states in the FSA.
     * @return True if the states are valid, false otherwise.
     */
    boolean validate(short numStates); //Ensures that all transitions are valid
}