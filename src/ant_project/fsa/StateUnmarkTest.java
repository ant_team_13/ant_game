package ant_project.fsa;

import ant_project.fsa.StateUnmark;
import org.junit.Test;

import static org.junit.Assert.*;

public class StateUnmarkTest {

    @Test
    public void testConstructor() throws Exception {
        //Test parameter rejection
        {boolean exc = false; try { new StateUnmark("0", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateUnmark("5", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateUnmark("1", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateUnmark("2", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateUnmark("6", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("-1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("a", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark(".", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("!", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("1", "a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("1", "."); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("1", "!"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("1", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("-100", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateUnmark("-1000000", "1"); } catch (Exception e) {exc = true;} assert(exc);}
    }

    @Test
    public void testValidate() throws Exception {
        {
            StateUnmark st = new StateUnmark("0","100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateUnmark st = new StateUnmark("1", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(st.validate((short)1));
        }
        {
            StateUnmark st = new StateUnmark("1", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
    }
}