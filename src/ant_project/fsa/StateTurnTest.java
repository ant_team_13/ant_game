package ant_project.fsa;

import ant_project.fsa.StateTurn;
import org.junit.Test;

import static org.junit.Assert.*;

public class StateTurnTest {

    @Test
    public void testConstructor() throws Exception {
        //Test parameter rejection
        {boolean exc = false; try { new StateTurn("Left", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateTurn("Right", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateTurn("Left", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateTurn("Right", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateTurn("Rith", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("-1", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("a", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn(".", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("!", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("Right", "a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("Right", "."); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("Right", "!"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("1", ""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("-Right", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateTurn("RightLeft", "1"); } catch (Exception e) {exc = true;} assert(exc);}
    }

    @Test
    public void testValidate() throws Exception {
        {
            StateTurn st = new StateTurn("Right","100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateTurn st = new StateTurn("Right", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(st.validate((short)1));
        }
        {
            StateTurn st = new StateTurn("Right", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
    }

}