package ant_project.fsa;

import ant_project.fsa.StateMove;
import org.junit.Test;

import static org.junit.Assert.*;

public class StateMoveTest {

    @Test
    public void testConstructor() throws Exception {
        //Test parameter rejection
        {boolean exc = false; try { new StateMove("1", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMove("1", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMove("0", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMove("9999", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMove("0", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMove("9999", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StateMove("a", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMove(".", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMove("!", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMove("1", "a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMove("1", "."); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMove("1", "!"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMove("", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StateMove("1", ""); } catch (Exception e) {exc = true;} assert(exc);}
    }

    @Test
    public void testValidate() throws Exception {
        {
            StateMove st = new StateMove("100", "100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateMove st = new StateMove("0", "100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateMove st = new StateMove("100", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StateMove st = new StateMove("0", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(st.validate((short)1));
        }
        {
            StateMove st = new StateMove("-1", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
        {
            StateMove st = new StateMove("0", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
        {
            StateMove st = new StateMove("-1", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
    }

}