package ant_project.fsa;

import ant_project.fsa.StatePickup;
import org.junit.Test;

import static org.junit.Assert.*;

public class StatePickupTest {

    @Test
    public void testConstructor() throws Exception {
        //Test parameter rejection
        {boolean exc = false; try { new StatePickup("1", "1"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StatePickup("1", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StatePickup("0", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StatePickup("9999", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StatePickup("0", "9999"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StatePickup("9999", "0"); } catch (Exception e) {exc = true;} assert(!exc);}
        {boolean exc = false; try { new StatePickup("a", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StatePickup(".", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StatePickup("!", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StatePickup("1", "a"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StatePickup("1", "."); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StatePickup("1", "!"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StatePickup("", "1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { new StatePickup("1", ""); } catch (Exception e) {exc = true;} assert(exc);}
    }

    @Test
    public void testValidate() throws Exception {
        {
            StatePickup st = new StatePickup("100", "100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StatePickup st = new StatePickup("0", "100");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StatePickup st = new StatePickup("100", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-100));
            assert(!st.validate((short)100));
            assert(st.validate((short)101));
        }
        {
            StatePickup st = new StatePickup("0", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(st.validate((short)1));
        }
        {
            StatePickup st = new StatePickup("-1", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
        {
            StatePickup st = new StatePickup("0", "-1");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
        {
            StatePickup st = new StatePickup("-1", "0");
            assert(!st.validate((short)0));
            assert(!st.validate((short)-1));
            assert(!st.validate((short)1));
        }
    }
}