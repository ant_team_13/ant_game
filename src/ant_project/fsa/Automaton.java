/**
 * Created by Daniel Saska on 22/02/2015.
 */
package ant_project.fsa;

import ant_project.Ant;
import ant_project.Map;

/**
 * Class representing the Finite State Automaton of Ant Brain
 */
public class Automaton {
    State[] states;

    /**
     * Creates the finite state automaton from code string
     * @param codeString The code string which is to be compiled to FSA.
     */
    public Automaton(String codeString) {
        this.states = Compiler.compile(codeString);
        validate();
    }

    /**
     * Confirms that the states of the FSA have correct transitions.
     */
    private void validate() {
        for (int i = 0; i < states.length; ++i) {
            State state = states[i];
            if(!state.validate((short) states.length)) {
                throw new IllegalArgumentException("State " + Integer.toString(i+1) + " has incorrect " +
                        "transition(s) to other states");
            }
        }
    }

    /**
     * Executes next state for ant on map.
     * @param ant Ant for which the state is to be executed.
     * @param map Map on which the game is played.
     */
    public void execute(Ant ant, Map map) {

        if(ant.getResting() > 0) {
            ant.setResting(ant.getResting()-1);
        } else {
            states[ant.getState()].execute(ant, map);
        }
    }
}
