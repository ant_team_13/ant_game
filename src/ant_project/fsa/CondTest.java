package ant_project.fsa;

import ant_project.fsa.State;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CondTest {

    @Test
    public void testFromStr() throws Exception {
        assertEquals(State.Cond.fromStr("Friend"), State.Cond.FRIEND);
        assertEquals(State.Cond.fromStr("Foe"), State.Cond.FOE);
        assertEquals(State.Cond.fromStr("FriendWithFood"), State.Cond.FRIEND_WITH_FOOD);
        assertEquals(State.Cond.fromStr("FoeWithFood"), State.Cond.FOE_WITH_FOOD);
        assertEquals(State.Cond.fromStr("Food"), State.Cond.FOOD);
        assertEquals(State.Cond.fromStr("Rock"), State.Cond.ROCK);
        assertEquals(State.Cond.fromStr("Marker"), State.Cond.MARKER);
        assertEquals(State.Cond.fromStr("FoeMarker"), State.Cond.FOE_MARKER);
        assertEquals(State.Cond.fromStr("Home"), State.Cond.HOME);
        assertEquals(State.Cond.fromStr("FoeHome"), State.Cond.FOE_HOME);

        //Rejection tests
        {boolean exc = false; try { State.Cond.fromStr("friend"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("FRIEND"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("foe"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("FOE"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("friendwithfood"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("FRIENDWITHFOOD"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("foewithfood"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("FOEWITHFOOD"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("food"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("FOOD"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("rock"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("ROCK"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("marker"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("MARKER"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("foemarker"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("FOEMARKER"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("home"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("HOME"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("foehome"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("FOEHOME"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr(""); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("1"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("Herea"); } catch (Exception e) {exc = true;} assert(exc);}
        {boolean exc = false; try { State.Cond.fromStr("Her"); } catch (Exception e) {exc = true;} assert(exc);}

    }
}